open_loop = false; % Enables open loop simulation
prof_enabled = false; % Enable profilling of control algorithm
model_uncert = true; % Enable the insertion of model uncertainties
sim_time = 15; % Time of simulation in seconds
roll_enabled = true; % Enables the roll control
horizon = 50; %MPC horizon

sample_time = 8e-4;
maneuver.name = 'doublelane'
%% Vehicle simulation parameters
model_friction_coeff = 0.75;
model_long_speed = speed;
a =  1.1; %length from cg to front axis
b =  1.3; %length from cg to rear axis
t_f =  1.4; %front track, length of front axis
t_r =  1.41; %rear track, length of rear axis
hg =  0.6; %cg heigh
m =  1070; %Total mass
hs =  0.55; %vertical heigh from roll center to cg
ms =  900; %sprunged mass, mass above roll center
kf =  65590/2; %roll stiffnesfs coefficient of front axis
kr =  65590/2; %roll  stiffness coefficient of rear axis
cf =  2100/2; %roll damping coefficient of front axis
cr =  2100/2; %roll damping coefficient of rear axis
roll_damping =  cf+cr; %roll damping
roll_stiffness =  kf+kr; %roll stiffness
Izz =  2100; %yaw inertial moment 
Ixx =  500; %roll  inertial moment
Ixz =  47.5; %Inertial product of yaw and roll axis
g =  9.80665 ; %Gravity acceleration
L = a+b; %Length from rear axis to front axis
% Coefficients of Pacejka`s Magic Formula
MFA = [ 1.3  ,-49 , 1216 ,  1632  , 11 ,     0.006 , -0.004 , -0.4, 0.003, -0.002, 0, -11, 0.045, 0, 0 ];
MFB = [ 1.57 ,-48   , 1338 ,   5.8   , 444  , 0  , 0.003       , -0.008      ,0.66,0,0 ];
Reff = 0.307; %Tire effective radius
Iw = 1; %Wheel inertial moment
front_steer_grad = -0.1; %Steer-by-roll coefficient of front axis
rear_steer_grad = 0.1; %Steer-by-roll coefficient of rear axis
% Paramenters of steering system UNUSED
Ifw = 3; 
Kss = 6300;
steer_by_roll = -0.1;
h_steer = -100;
xc = 0.0066;
camber_by_roll =-0.1;

%% Configurationf o vehicle model
% insertion of model uncertainties
if model_uncert
    delta_cg = L/40;
    delta_m = 0.1*m;
    delta_ms = 0.1*ms;
    delta_fri = -0.1*model_friction_coeff;
else
    delta_cg = 0;
    delta_m = 0;
    delta_fri = 0;
    delta_ms = 0;
end
    
veh_model = vehicle_settings(a-delta_cg,b+delta_cg,t_f,t_r,hg,m+delta_m,hs,ms+delta_ms,...
                       kf,kr,cf,cr,roll_damping,roll_stiffness,...
                       Izz,Ixx,Ixz,g,L,MFA,MFB,Reff,Iw,camber_by_roll,...
                       model_friction_coeff+delta_fri,model_long_speed,Ifw,Kss,...
                       steer_by_roll,xc,h_steer,front_steer_grad,rear_steer_grad);

%% Configuration of driver_model model
W = 0.2;
La = 20*(model_long_speed)/(60/3.6);
Tk = 0.2;
driver_model = driver_settings(W,La,Tk,model_long_speed);

%% Configuration of maneuver
pathg = pathgen(maneuver.name,10000,125,veh_model.t_r); 
maneuver.path_way = pathg(:,1:2);
maneuver.path_lb = pathg(:,3);
maneuver.path_ub = pathg(:,4);

%% Control settings
nuc = 1;
npc = 2;
Tcontrol = 13.6e-3;    
control_long_speed = 100/3.6;
control_friction_coeff = 0.5;
param_coeff = [ vars(1); npc; vars(2) ]; % MPC exp. param. coefficient
dc_level = 0; %Disable a dc level in exp. param.
npc = npc+dc_level;

% control activiation conditions
yaw_rate_err_th = 0.01;
beta_th = 0.1;
deactive_periods = 100;

nu = 1;
% constraints
if roll_enabled
   C_C =  [ 1 0 0 0 ]; %Enable restriction of the first state (slip angle)
else
   C_C = [1 0];
end
lb = -150*pi/180; %Lower constraint of slip angle
ub = -lb; %Upper constraint of slip angle
slew_lb = -2500*Tcontrol*ones(nu,1);%Lower constraint of difference between consecutive command updates. 
slew_ub = 2500*Tcontrol*ones(nu,1); %Upper m�xima entre duas amostras consecutivas do comando
cmd_lb = -250*ones(nu,1); %Upper constraint of command
cmd_ub = 250*ones(nu,1); %Lower constraint of command

if roll_enabled 
    Q_Y = diag(vars(3:end));
    controlled_states = [2 4];
else
    Q_Y = vars(3);
    controlled_states = 2;
end

Q_U = 1e-5;

%control model
control_model = control_settings(a,b,t_f,t_r,hg,m,hs,ms,kf,kr,cf,cr,roll_damping,roll_stiffness,...
                              Izz,Ixx,Ixz,g,L,MFA,MFB,Reff,Iw,camber_by_roll,...
                              control_friction_coeff,control_long_speed,Q_Y,Q_U,Tcontrol,...
                              yaw_rate_err_th,beta_th,deactive_periods,lb,ub,slew_lb,slew_ub,...
                              cmd_lb,cmd_ub,horizon,C_C,param_coeff,dc_level,roll_enabled,controlled_states);