function out_desired_yaw_rate  = desired_states( arg_steer_angle, arg_speed, mu_g, L, Ku)

speed = arg_speed;
aux = (1 + Ku*speed*speed)*L;
desired_yaw_rate = speed*arg_steer_angle/aux;


out_desired_yaw_rate = min(abs(desired_yaw_rate), abs(mu_g/speed)).*sign(arg_steer_angle);

end