function bio=xpcvehiclebio
bio = [];
bio(1).blkName='Estados lidos e comando enviado  pelo controlador/ Leitura do comando/p1';
bio(1).sigName='';
bio(1).portIdx=0;
bio(1).dim=[8,1];
bio(1).sigWidth=8;
bio(1).sigAddress='&xpcvehicle_B.Leituradocomando_o1[0]';
bio(1).ndims=2;
bio(1).size=[];

bio(getlenBIO) = bio(1);

bio(2).blkName='Estados lidos e comando enviado  pelo controlador/ Leitura do comando/p2';
bio(2).sigName='';
bio(2).portIdx=1;
bio(2).dim=[1,1];
bio(2).sigWidth=1;
bio(2).sigAddress='&xpcvehicle_B.Leituradocomando_o2';
bio(2).ndims=2;
bio(2).size=[];


bio(3).blkName='Estados lidos e comando enviado  pelo controlador/Byte Packing 1';
bio(3).sigName='';
bio(3).portIdx=0;
bio(3).dim=[1,1];
bio(3).sigWidth=1;
bio(3).sigAddress='&xpcvehicle_B.BytePacking1';
bio(3).ndims=2;
bio(3).size=[];


bio(4).blkName='PCI-DAC6703/MATLAB Function1';
bio(4).sigName='out_slip_angle';
bio(4).portIdx=0;
bio(4).dim=[1,1];
bio(4).sigWidth=1;
bio(4).sigAddress='&xpcvehicle_B.out_slip_angle';
bio(4).ndims=2;
bio(4).size=[];


bio(5).blkName='PCI-DAC6703/Conv_1';
bio(5).sigName='';
bio(5).portIdx=0;
bio(5).dim=[1,1];
bio(5).sigWidth=1;
bio(5).sigAddress='&xpcvehicle_B.Conv_1';
bio(5).ndims=2;
bio(5).size=[];


bio(6).blkName='PCI-DAC6703/Conv_2';
bio(6).sigName='';
bio(6).portIdx=0;
bio(6).dim=[1,1];
bio(6).sigWidth=1;
bio(6).sigAddress='&xpcvehicle_B.Conv_2';
bio(6).ndims=2;
bio(6).size=[];


bio(7).blkName='PCI-DAC6703/Conv_3';
bio(7).sigName='';
bio(7).portIdx=0;
bio(7).dim=[1,1];
bio(7).sigWidth=1;
bio(7).sigAddress='&xpcvehicle_B.Conv_3';
bio(7).ndims=2;
bio(7).size=[];


bio(8).blkName='PCI-DAC6703/Conv_4';
bio(8).sigName='';
bio(8).portIdx=0;
bio(8).dim=[1,1];
bio(8).sigWidth=1;
bio(8).sigAddress='&xpcvehicle_B.Conv_4';
bio(8).ndims=2;
bio(8).size=[];


bio(9).blkName='PCI-DAC6703/Conv_5';
bio(9).sigName='';
bio(9).portIdx=0;
bio(9).dim=[1,1];
bio(9).sigWidth=1;
bio(9).sigAddress='&xpcvehicle_B.Conv_5';
bio(9).ndims=2;
bio(9).size=[];


bio(10).blkName='PCI-DAC6703/Saturation1';
bio(10).sigName='';
bio(10).portIdx=0;
bio(10).dim=[1,1];
bio(10).sigWidth=1;
bio(10).sigAddress='&xpcvehicle_B.Saturation1';
bio(10).ndims=2;
bio(10).size=[];


bio(11).blkName='PCI-DAC6703/Saturation2';
bio(11).sigName='';
bio(11).portIdx=0;
bio(11).dim=[1,1];
bio(11).sigWidth=1;
bio(11).sigAddress='&xpcvehicle_B.Saturation2';
bio(11).ndims=2;
bio(11).size=[];


bio(12).blkName='PCI-DAC6703/Saturation3';
bio(12).sigName='';
bio(12).portIdx=0;
bio(12).dim=[1,1];
bio(12).sigWidth=1;
bio(12).sigAddress='&xpcvehicle_B.Saturation3';
bio(12).ndims=2;
bio(12).size=[];


bio(13).blkName='PCI-DAC6703/Saturation4';
bio(13).sigName='';
bio(13).portIdx=0;
bio(13).dim=[1,1];
bio(13).sigWidth=1;
bio(13).sigAddress='&xpcvehicle_B.Saturation4';
bio(13).ndims=2;
bio(13).size=[];


bio(14).blkName='PCI-DAC6703/Saturation5';
bio(14).sigName='';
bio(14).portIdx=0;
bio(14).dim=[1,1];
bio(14).sigWidth=1;
bio(14).sigAddress='&xpcvehicle_B.Saturation5';
bio(14).ndims=2;
bio(14).size=[];


bio(15).blkName='Simula��o dos  movimentos do ve�culo/Constant';
bio(15).sigName='';
bio(15).portIdx=0;
bio(15).dim=[1,1];
bio(15).sigWidth=1;
bio(15).sigAddress='&xpcvehicle_B.Constant';
bio(15).ndims=2;
bio(15).size=[];


bio(16).blkName='Simula��o dos  movimentos do ve�culo/Memory';
bio(16).sigName='';
bio(16).portIdx=0;
bio(16).dim=[4,1];
bio(16).sigWidth=4;
bio(16).sigAddress='&xpcvehicle_B.Memory[0]';
bio(16).ndims=2;
bio(16).size=[];


bio(17).blkName='Simula��o dos  movimentos do ve�culo/Rate Transition';
bio(17).sigName='';
bio(17).portIdx=0;
bio(17).dim=[1,1];
bio(17).sigWidth=1;
bio(17).sigAddress='&xpcvehicle_B.RateTransition';
bio(17).ndims=2;
bio(17).size=[];


bio(18).blkName='Simula��o dos  movimentos do ve�culo/Rate Limiter';
bio(18).sigName='';
bio(18).portIdx=0;
bio(18).dim=[1,1];
bio(18).sigWidth=1;
bio(18).sigAddress='&xpcvehicle_B.RateLimiter';
bio(18).ndims=2;
bio(18).size=[];


bio(19).blkName='Simula��o dos  movimentos do ve�culo/Saturation';
bio(19).sigName='';
bio(19).portIdx=0;
bio(19).dim=[1,1];
bio(19).sigWidth=1;
bio(19).sigAddress='&xpcvehicle_B.Saturation';
bio(19).ndims=2;
bio(19).size=[];


bio(20).blkName='Simula��o dos  movimentos do ve�culo/Vehicle';
bio(20).sigName='';
bio(20).portIdx=0;
bio(20).dim=[4,1];
bio(20).sigWidth=4;
bio(20).sigAddress='&xpcvehicle_B.Vehicle[0]';
bio(20).ndims=2;
bio(20).size=[];


bio(21).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/MATLAB Function1/p1';
bio(21).sigName='out_vy';
bio(21).portIdx=0;
bio(21).dim=[1,1];
bio(21).sigWidth=1;
bio(21).sigAddress='&xpcvehicle_B.out_vy';
bio(21).ndims=2;
bio(21).size=[];


bio(22).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/MATLAB Function1/p2';
bio(22).sigName='out_vx';
bio(22).portIdx=1;
bio(22).dim=[1,1];
bio(22).sigWidth=1;
bio(22).sigAddress='&xpcvehicle_B.out_vx';
bio(22).ndims=2;
bio(22).size=[];


bio(23).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/preview path/p1';
bio(23).sigName='out_y';
bio(23).portIdx=0;
bio(23).dim=[1,1];
bio(23).sigWidth=1;
bio(23).sigAddress='&xpcvehicle_B.out_y';
bio(23).ndims=2;
bio(23).size=[];


bio(24).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/preview path/p2';
bio(24).sigName='out_lb';
bio(24).portIdx=1;
bio(24).dim=[1,1];
bio(24).sigWidth=1;
bio(24).sigAddress='&xpcvehicle_B.out_lb';
bio(24).ndims=2;
bio(24).size=[];


bio(25).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/preview path/p3';
bio(25).sigName='out_ub';
bio(25).portIdx=2;
bio(25).dim=[1,1];
bio(25).sigWidth=1;
bio(25).sigAddress='&xpcvehicle_B.out_ub';
bio(25).ndims=2;
bio(25).size=[];


bio(26).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Gain';
bio(26).sigName='';
bio(26).portIdx=0;
bio(26).dim=[1,1];
bio(26).sigWidth=1;
bio(26).sigAddress='&xpcvehicle_B.Gain';
bio(26).ndims=2;
bio(26).size=[];


bio(27).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Integrator';
bio(27).sigName='';
bio(27).portIdx=0;
bio(27).dim=[1,1];
bio(27).sigWidth=1;
bio(27).sigAddress='&xpcvehicle_B.Integrator';
bio(27).ndims=2;
bio(27).size=[];


bio(28).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Integrator1';
bio(28).sigName='';
bio(28).portIdx=0;
bio(28).dim=[1,1];
bio(28).sigWidth=1;
bio(28).sigAddress='&xpcvehicle_B.Integrator1';
bio(28).ndims=2;
bio(28).size=[];


bio(29).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Integrator2';
bio(29).sigName='';
bio(29).portIdx=0;
bio(29).dim=[1,1];
bio(29).sigWidth=1;
bio(29).sigAddress='&xpcvehicle_B.Integrator2';
bio(29).ndims=2;
bio(29).size=[];


bio(30).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Saturation';
bio(30).sigName='';
bio(30).portIdx=0;
bio(30).dim=[1,1];
bio(30).sigWidth=1;
bio(30).sigAddress='&xpcvehicle_B.Saturation_m';
bio(30).ndims=2;
bio(30).size=[];


bio(31).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Sum';
bio(31).sigName='';
bio(31).portIdx=0;
bio(31).dim=[1,1];
bio(31).sigWidth=1;
bio(31).sigAddress='&xpcvehicle_B.Sum';
bio(31).ndims=2;
bio(31).size=[];


bio(32).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Sum1';
bio(32).sigName='';
bio(32).portIdx=0;
bio(32).dim=[1,1];
bio(32).sigWidth=1;
bio(32).sigAddress='&xpcvehicle_B.Sum1';
bio(32).ndims=2;
bio(32).size=[];


bio(33).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System/Internal';
bio(33).sigName='';
bio(33).portIdx=0;
bio(33).dim=[1,1];
bio(33).sigWidth=1;
bio(33).sigAddress='&xpcvehicle_B.Internal';
bio(33).ndims=2;
bio(33).size=[];


bio(34).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System1/Internal';
bio(34).sigName='';
bio(34).portIdx=0;
bio(34).dim=[1,1];
bio(34).sigWidth=1;
bio(34).sigAddress='&xpcvehicle_B.Internal_f';
bio(34).ndims=2;
bio(34).size=[];


bio(35).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System2/Internal';
bio(35).sigName='';
bio(35).portIdx=0;
bio(35).dim=[1,1];
bio(35).sigWidth=1;
bio(35).sigAddress='&xpcvehicle_B.Internal_h';
bio(35).ndims=2;
bio(35).size=[];


bio(36).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System3/Internal';
bio(36).sigName='';
bio(36).portIdx=0;
bio(36).dim=[1,1];
bio(36).sigWidth=1;
bio(36).sigAddress='&xpcvehicle_B.Internal_b';
bio(36).ndims=2;
bio(36).size=[];


bio(37).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System4/Internal';
bio(37).sigName='';
bio(37).portIdx=0;
bio(37).dim=[1,1];
bio(37).sigWidth=1;
bio(37).sigAddress='&xpcvehicle_B.Internal_k';
bio(37).ndims=2;
bio(37).size=[];


bio(38).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System/Input Delay/Transport Delay';
bio(38).sigName='';
bio(38).portIdx=0;
bio(38).dim=[1,1];
bio(38).sigWidth=1;
bio(38).sigAddress='&xpcvehicle_B.TransportDelay';
bio(38).ndims=2;
bio(38).size=[];


bio(39).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System1/Input Delay/Transport Delay';
bio(39).sigName='';
bio(39).portIdx=0;
bio(39).dim=[1,1];
bio(39).sigWidth=1;
bio(39).sigAddress='&xpcvehicle_B.TransportDelay_i';
bio(39).ndims=2;
bio(39).size=[];


bio(40).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System2/Input Delay/Transport Delay';
bio(40).sigName='';
bio(40).portIdx=0;
bio(40).dim=[1,1];
bio(40).sigWidth=1;
bio(40).sigAddress='&xpcvehicle_B.TransportDelay_d';
bio(40).ndims=2;
bio(40).size=[];


bio(41).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System3/Input Delay/Transport Delay';
bio(41).sigName='';
bio(41).portIdx=0;
bio(41).dim=[1,1];
bio(41).sigWidth=1;
bio(41).sigAddress='&xpcvehicle_B.TransportDelay_j';
bio(41).ndims=2;
bio(41).size=[];


bio(42).blkName='Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System4/Input Delay/Transport Delay';
bio(42).sigName='';
bio(42).portIdx=0;
bio(42).dim=[1,1];
bio(42).sigWidth=1;
bio(42).sigAddress='&xpcvehicle_B.TransportDelay_o';
bio(42).ndims=2;
bio(42).size=[];


function len = getlenBIO
len = 42;

