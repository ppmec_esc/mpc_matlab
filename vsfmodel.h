#ifndef _vsfmodel_
#define _vsfmodel_

#define V_ID (0)
#define YAW_RATE_ID (1)
#define ROLL_RATE_ID (2)
#define ROLL_ID (3)
#define STEER_RATE_ID (4)
#define STEER_ID (5)

#define NOF_STATES (4)

#define V          xC[V_ID]
#define YAW_RATE   xC[YAW_RATE_ID]
#define ROLL_RATE  xC[ROLL_RATE_ID]
#define ROLL       xC[ROLL_ID]
#define STEER_RATE xC[STEER_RATE_ID]
#define STEER      xC[STEER_ID]

#define LAST_V          outStates[V_ID]
#define LAST_YAW_RATE   outStates[YAW_RATE_ID]
#define LAST_ROLL_RATE  outStates[ROLL_RATE_ID]
#define LAST_ROLL       outStates[ROLL_ID]

#define FL (0)
#define FR (1)
#define RL (2)
#define RR (3)

#define MFA_POS    (0)
#define MFA_LENGTH (15)
#define MFA	       (&params[MFA_POS])
#define MFB_POS    (MFA_POS+MFA_LENGTH)
#define MFB_LENGTH (11)
#define MFB        (&params[MFB_POS])
#define Z_POS      (MFB_POS+MFB_LENGTH)
#define Z_LENGTH   (7)
#define Z          (&params[Z_POS])
#define ZF0_POS    (Z_POS + Z_LENGTH)
#define ZF0        (params[ZF0_POS])
#define ZR0_POS    (ZF0_POS+1)
#define ZR0        (params[ZR0_POS])
#define A_POS      (ZR0_POS + 1)
#define A          (params[A_POS])
#define B_POS      (A_POS+1)
#define B          (params[B_POS])
#define T2_POS     (B_POS+1)
#define TF2        (params[T2_POS])
#define TR2_POS    (T2_POS+1)
#define TR2        (params[TR2_POS])
#define D_POS      (TR2_POS+1)
#define D_LENGTH   (15)
#define D          (&params[D_POS])
#define REFF_POS   (D_POS+D_LENGTH)
#define REFF       (params[REFF_POS])
#define FRICTION_POS (REFF_POS+1)
#define FRICTION     (params[FRICTION_POS])
#define CONST_SPEED_POS (FRICTION_POS+1)
#define CONST_SPEED     (params[CONST_SPEED_POS])
#define CAMBER_BY_ROLL_POS (CONST_SPEED_POS+1)
#define CAMBER_BY_ROLL     (params[CAMBER_BY_ROLL_POS])
#define IFW_POS (CAMBER_BY_ROLL_POS+1)
#define IFW (params[IFW_POS])
#define KSS_POS (IFW_POS+1)
#define KSS (params[KSS_POS])
#define STEER_BY_ROLL_POS (KSS_POS+1)
#define STEER_BY_ROLL (params[STEER_BY_ROLL_POS])
#define XC_POS (STEER_BY_ROLL_POS+1)
#define XC (params[XC_POS])
#define H_STEER_POS (XC_POS+1)
#define H_STEER (params[H_STEER_POS])
#define FRONT_STEER_BY_ROLL_POS (H_STEER_POS+1)
#define FRONT_STEER_BY_ROLL (params[FRONT_STEER_BY_ROLL_POS])
#define REAR_STEER_BY_ROLL_POS (FRONT_STEER_BY_ROLL_POS+1)
#define REAR_STEER_BY_ROLL (params[REAR_STEER_BY_ROLL_POS])

#define LAT_ACCEL  ( dx[V_ID] )
#define LONG_ACCEL ( 0 )

#define sind(x) (sin(fmod((x),360) * M_PI / 180))
#define tand(x) (tan(fmod((x),360) * M_PI / 180))
#define atand(x) (fmod(atan(x)*180/M_PI,360) )
#define atan2d(x,y) (fmod(atan2(x,y)*180/M_PI,360) )


#endif