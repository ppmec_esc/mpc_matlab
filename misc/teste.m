%{
---------------------------------------------------------------------------
  Criador: Zo� Magalh�es (zr.magal@gmail.com)
  Mestrando do PPMEC-Unb, matr�cula 170172767
  Disciplina: Controle Preditivo 01/2018
  Professor: Andr� Murilo
  
  Este script executa uma simula��o para o controle de estabilidade lateral 
  veicular mediante aplica��o do controle preditivo para sistemas LTI,
  com perturba��o mensur�vel (entrada n�o controlada) e restri��es
  para os estados e sinal de controle.
%}

clear all
%% Configura��o
   
    %Horizonte de predi��o
    simlinear = [];
    openloop = 0;
    Horizon = 100;
    Tcontrol = 2e-2;
    Ts = 8e-4;
    
    control_long_speed = 80/3.6;
        
    control_friction_coeff = 0.75;
    model_friction_coeff = 0.75;
    
    model_long_speed = 100/3.6;
%   
%     %Driver model OPA
%     Tp = 1.213%0.72;
%     Tc = 0.8027%0.52;
%     Td = 0.2826%0.1;
%     Tn = 0.2045%0.2;
% %     driverTF = tf([Tc 1],[Tn 1])*exp(-Td*s);
%     s = zpk('s');
%     pathDelayTF = exp(-Tp*s);

%Driver model Andrzej
    W = 0.28
    La = 35
    Tk = 0.25
    Tp = (La/model_long_speed) - Tk;
    s=zpk('s');
    PREVIEWLOG = tf([1],[1],'InputDelay',Tp);
    VY_GAIN = W/La;
    VY_FB = tf([W/La],[1 0],'InputDelay',Tk);
    YAW_FB = tf([W],[1],'InputDelay',Tk);
       
    % Manobra de teste
    maneuver = 'doublelane'; %'doublelane'
    %max_steer = 15*pi/180;

    
    %control enable conditions
    yaw_rate_err_th = 0.5*pi/180;
    beta_th = 3*pi/180;
    
    param_coeff = [ 1.1; 2; 20 ]; %Parametriza��o exponencial lambda;alpha; ne 
    dc_level = 0;
    
    % Pondera��es da fun��o custo
    Q_Y = diag(1e12); % Pondera��o do erro em y
    Q_U = [1];
    Q_U = diag(Q_U);
    Q_U = Q_U/norm(Q_U);
    nu = 1;
    
    % Restri��es 
    C_C =  [ 1 0 0 0 ]; %Restringe apenas o primeiro estado (deslizamento)

    lb = -5*pi/180;            %Valor m�nimo do estado regulado (slip angle)
    ub = -lb;               %Valor m�ximo do estado reguladoo
    slew_lb = -200*ones(nu,1);        %Diferen�a m�nima entre duas amostras consecutivas do comando
    slew_ub = 200*ones(nu,1);         %Diferen�a m�xima entre duas amostras consecutivas do comando
    cmd_lb = -500*ones(nu,1);         %Valor m�ximo do comandos
    cmd_ub = 500*ones(nu,1);          %Valor m�nimo do comando
    rate_factor = 100;       %Fator utilizado na atualizacao dinamica da restricao do slip rate
    
    rate_th =  pi*Tcontrol/180;   %Limiar para nao restringir a variacao do slip_rate em zero.
    
    fac = 1.0;
    
    csvwrite('cmd_ub.csv',cmd_ub);
    csvwrite('cmd_lb.csv',cmd_lb);

%% Par�metros do modelo
    a                   =  1.1;   %dist�ncia entre cg e eixo frontal
    b                   =  1.3;   %dist�ncia entre cg e eixo traseiropl
    tf                  =  1.4;   %comprimento do eixo frontal
    tr                  =  1.41;  %comprimento do eixo traseiro
    hg                  =  0.6;   %altura do cg
    m                   =  1070;  %massa
    hs                  =  0.55;  %altura do centro de rolagem
    ms                  =  900;    %massaa sobre centro de rolagem
    kf					=  65590/2;  %coeficiente de rigidez frontal a rolagem
    kr 					=  65590/2;  %coeficiente de rigidez traseira a rolagem
    cf					=  2100/2;   %coeficiente de amortecimento frontal a rolagem
    cr 					=  2100/2;   %coeficiente de amortecimental traseiro a rolagem
    roll_damping        =  cf+cr;   %coeficiente de rigidez a rolagem
    roll_stiffness      =  kf+kr;   %coeficiente de amortecimento a rolagem
    Izz                 =  2100;  %momento de in�rcia do eixo de guinada
    Ixx                 =  500;     %momento de in�rcia do eixo de rolagem
    Ixz                 =  47.5;      %produto de in�rcia dos eixos de rolagem e guinada
    g                   =  9.80665 ;    %acelera��o gravitacional
    L = a+b;                       %dist�ncia entre os eixos frontal e traseiro       
    uspeed = 80/3.6;               %velociade longitudinal de lineariza��o
    %MFA                 = [ 1.6  ,-34 , 1250 ,  2320  , 12.8 ,     0 , -0.0053 , 0.1925 ];
 	%MFB                 = [ 1.55 ,0   , 1000 ,   60   , 300  , 0.17  , 0       , 0      ,0.2 ];
    MFA                 = [ 1.3  ,-49 , 1216 ,  1623  , 11 ,     0.006 , -0.004 , -0.4 ];
 	MFB                 = [ 1.57 ,-48   , 1338 ,   5.8   , 444  , 0  , 0.003       , -0.008      ,0.66 ];
	Reff                = 0.307; 
    Iw                  = 1;
    camber_by_roll      = 0;

    m = fac*m;
    ms = fac*ms;
    Izz = fac*Izz;
    Ixz = fac*Ixz;
    Ixx = fac*Ixx;
    
    Fze = ([b,b,a,a]*m*g/(200*L));

    C_alpha = control_friction_coeff*MFA(4)*sind( 2*atand( Fze /MFA(5) ) )/(pi/180)
    Ku =abs(m*2*((a/(C_alpha(3)+C_alpha(4))) - (b/(C_alpha(1)+C_alpha(2))))/(L*L));

    model_wheel_speed = model_long_speed/Reff;
	pneumatic_trail     = 0;
 
	eta = m*Ixx*Izz - m*Ixz*Ixz - ms*ms*hs*hs*Izz;

	d(1) = ms*hs*Izz;
	d(2) = m*Ixz;
	d(3) = m*Izz*ms*hs*g;
	d(4) = m*Izz*roll_stiffness;
	d(5) = m*Izz*roll_damping;
	d(1:5) = d(1:5)/eta;
	d(6) = 1/Izz;
	d(7) = Ixz/Izz;
	d(8) = 1/m;
    d(9) = 1;
    d(10) = ms*hs/m;
	d(11) = 1/m;
	d(12) = ms*hs/m;
	d(13) = 1;
    d(14) = 1/Iw;
    d(15) = Reff/Iw;

	zf0 = m*g*b/(2*L);
	zr0 = m*g*a/(2*L);
	z(1) = m*hg*b/(2*L);
	z(2) = m*hg/(L*tf);
	z(3) = kf/tf;
	z(4) = cf/tf;
	z(5) = kr/tr;
	z(6) = cr/tr;
	z(7) = m*hg*a/(2*L);
	tf2 = tf/2;
    tr2 = tr/2;
    C_1 = 4;
    M_1 = 0.5;
    M_2 = 0.5;
    GAMMA = 8; 
    
    %states_max = [14.55 23.80 0.35 0.01 0.02]
    %states_min = [-12.31 6.18 -0.26 -0.01 -0.02];
    dac_max = [0.88,  0.22 , 0.13 , 0.042 , 0.03];
    dac_min = [-1.7 ,-0.44  ,-0.22 ,-0.09 ,-0.07];

	params = [MFA,MFB,z,zf0,zr0,a,b,tf2,tr2,d,Reff,model_friction_coeff,model_long_speed,camber_by_roll];
   
    fac = 1/fac;
    m = fac*m;
    ms = fac*ms;
    Izz = fac*Izz;
    Ixz = fac*Ixz;
    Ixx = fac*Ixx;
    
    mu_g = control_friction_coeff*g;
    
    
    G1 = [];
    G2 = [];
    G3 = [];
    G4 = [];
    F1 = [];
    F2 = [];
    F3 = [];
    AINEQ = [];
    H = [];

    %% Modelo simlinear Mx'= A1x + B1u + E1steer
    M1 = [      m*control_long_speed,    0,  -ms*hs,  0;
                       0,  Izz,    -Ixz,  0;
                -ms*hs*control_long_speed, -Ixz,     Ixx,  roll_damping;
                       0,    0,       0,  1 ];

    A1 = [-sum(C_alpha)         , -(C_alpha*[a;a;-b;-b]/control_long_speed) - m*control_long_speed  , 0  , 0;
          -C_alpha*[a;a;-b;-b]  , -C_alpha*[a*a;a*a;b*b;b*b]/control_long_speed              , 0  , 0;
            0                   ,  ms*hs*control_long_speed                           , 0  , (ms*hs*g-roll_stiffness);
            0                   , 0                                       , 1  , 0 ];

    B1 = [0; 1; 0; 0];  

    E1 = [ C_alpha*[1;1;0;0]; C_alpha*[a;a;0;0];0;0 ];

    %% Modelo espa�o de estado: x` = Ax + Bu + Esteer y'= Cx + Du
    A = M1\A1;
    B = M1\B1;
    C = eye(size(A));
    E = M1\E1;
    D = zeros(size([B E]));

    sys = ss( A,[B E],C,D);
    d_sys =c2d(sys,Tcontrol);

    csvwrite('dA.csv',d_sys.a);
    csvwrite('dB.csv',d_sys.b);

    if find( abs(eig(d_sys.a)) > 1.0 )
        discret_ss_unstable = abs(eig(d_sys.a))
        return
    end

    %codegen -report model_update.m -args {zeros(size(d_sys.a)), zeros(size(d_sys.b)), zeros(4,1),0,0}

    %% Criando um descritor do controle preditivo
    dA = d_sys.a;
    dB = d_sys.b;
    dC = d_sys.c;
    dD = d_sys.d;

    mpcobj = ClassLinearMPC.settings(d_sys.a,d_sys.b(:,1:end-size(E,2)), d_sys.c(2,:), d_sys.d(:,1:end-size(E,2)),...
             Q_U,Q_Y, C_C, Horizon, ...
             lb, ub, ...
             slew_lb,slew_ub,...
             cmd_lb, cmd_ub,...
             d_sys.b(:,end-size(E,2)+1:end), ...
             'exponencial', param_coeff, dc_level, Tcontrol );

    G1 = mpcobj.G1;
    G2 = mpcobj.G2;
    G3 = mpcobj.G3;
    G4 = mpcobj.G4;
    F1 = mpcobj.F1;
    F2 = mpcobj.F2;
    F3 = mpcobj.F3;
    AINEQ = mpcobj.AINEQ();
    H = mpcobj.H;

    np = mpcobj.np;
    nc = mpcobj.nc;
    nu = mpcobj.nu;
    nv = mpcobj.nv;
    nuc = nu - nv;
    N = Horizon;

    ns = size(A,2);

    PI = mpcobj.PI_E;

    clear mpcobj    
    clear sys
    clear d_sys

    %% Carrega os sinais para o ester�amento das rodas dianteiras
    switch maneuver 
        case 'fishhook'
        load simin;
        disp('fishhook')
        steer = simin(1:8:300001,:);

        case 'doublelane'
        %load mats/dlcpath2
        dlc_maxx = 125;
        dlc_maxy = (3.5 - ((1.1*tr + 0.25)/2)) + (1.2*tr+0.25)/2
        dlc_th1 = (1.1*tr+0.25)/2;
        dlc_th2 = (1.2*tr+0.25)/2;
        dlc_th3 = (1.3*tr+0.25)/2;
        nof_pts = 10000;
        path1 = 15;
        path2 = path1 + 30;
        path3 = path2 + 25;
        path4 = path3 + 25;
        path5 = path4 + 30;
        
        dlc_x = dlc_maxx*([0:(nof_pts-1)])/nof_pts;
        dlc_y = dlc_x*0;
        dlc_lb = dlc_y;
        dlc_ub = dlc_y;
        
        i = floor(path1*nof_pts/dlc_maxx);
        f = floor(path2*nof_pts/dlc_maxx);
        x = ([i:f] - ((i+f)/2))/((i+f)/2);
        sigpath = 1./(1+exp(-10*x));
        sigpath = sigpath - min(sigpath);
        sigpath = sigpath/max(sigpath);
        dlc_y(i:f) = sigpath;
        i = f;
        f = floor(path3*nof_pts/dlc_maxx);
        dlc_y(i:f) = 1;
        i = f;
        f = floor(path4*nof_pts/dlc_maxx);
        x = ([i:f] - ((i+f)/2))/((i+f)/2);
        x = -x;
        sigpath = 1./(1+exp(-30*x));
        sigpath = sigpath - min(sigpath);
        sigpath = sigpath/max(sigpath);
        dlc_y(i:f) = sigpath;
        i = f;
        f = floor(path5*nof_pts/dlc_maxx);
        dlc_y(i:f) = 0;
        dlc_y = dlc_y*dlc_maxy;
        
        i = 1;
        f = floor(path2*nof_pts/dlc_maxx);
        dlc_lb(i:f)= - dlc_th1;
        
        i = f;
        f = floor(path3*nof_pts/dlc_maxx);
        dlc_lb(i:f)=3.5 - dlc_th1;
        
        i = f;
        dlc_lb(i:end) = - dlc_th3;
        
        i = 1;
        f = floor(path1*nof_pts/dlc_maxx);
        dlc_ub(i:f) = dlc_th1;
        
        i=f;
        f = floor(path4*nof_pts/dlc_maxx);
        dlc_ub(i:f) = 3.5 + 2*dlc_th2 - dlc_th1;
        
        i=f;
        dlc_ub(i:end) = dlc_th3;
        
        
        
        path(:,1)=dlc_x;%dlcpath(:,1)*dlc_maxx/dlcpath(end,1);
        path(:,2)=dlc_y;%lcpath(:,2)*dlc_maxy/max(dlcpath(:,2));
        path_lb = dlc_lb;
        path_ub = dlc_ub;
       % load mats/dlcinput;
       % disp('doublelane')
       % steer = dlcinput;
       % steer(:,1) = steer(:,1);

        otherwise
            disp('entrada nao implementada')

    end

    sim('vscmpc');
    %plotpath
    %plotnonlinearresult
