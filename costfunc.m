function cost =  costfunc(vars)
    persistent globalcost;
    persistent globalsol;
    
    if isempty(globalcost)
        globalcost = 1e20;
        globalsol = 0;
    end

    speed = 80/3.6;
    simsettings;
    simresult = domaneuver(veh_model, driver_model, control_model, maneuver, sample_time, open_loop, sim_time, prof_enabled);
    cost = result2cost(simresult);
    
    if cost < globalcost
        globalsol = vars
        globalcost = cost
    end
end
