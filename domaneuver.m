function simresult = domaneuver(veh_model,driver_model, control_model, maneuver, sample_time, open_loop, sim_time, prof_enabled)         
    simresult = 0;
    % run simulation
    % sometimes simulink says model is unstable when it is not
    try
        simresult = run(veh_model,driver_model,control_model,maneuver.path_way,maneuver.path_lb,maneuver.path_ub,sample_time,open_loop,sim_time,prof_enabled);
    catch
        error = 'sim unstable, try again';
        bdclose
        try
           simresult = run(veh_model,driver_model,control_model,maneuver.path_way,maneuver.path_lb,maneuver.path_ub,sample_time,open_loop,sim_time,prof_enabled);
        catch
            error = 'sim unstable'     
        end 
    end
   
end
