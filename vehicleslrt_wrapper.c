

/*
 * Include Files
 *
 */
#if defined(MATLAB_MEX_FILE)
#include "tmwtypes.h"
#include "simstruc_types.h"
#else
#include "rtwtypes.h"
#endif

/* %%%-SFUNWIZ_wrapper_includes_Changes_BEGIN --- EDIT HERE TO _END */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "vsfmodel.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <simstruc.h>
#define FORCE_GAIN (1)
#define VERSION 1.1
/* %%%-SFUNWIZ_wrapper_includes_Changes_END --- EDIT HERE TO _BEGIN */
#define u_width 1
#define y_width 1
/*
 * Create external references here.  
 *
 */
/* %%%-SFUNWIZ_wrapper_externs_Changes_BEGIN --- EDIT HERE TO _END */
/* extern double func(double a); */
/* %%%-SFUNWIZ_wrapper_externs_Changes_END --- EDIT HERE TO _BEGIN */

/*
 * Output functions
 *
 */
void vehicleslrt_Outputs_wrapper(const real_T *inSteer,
			const real_T *inMz,
			real_T *outStates,
			const real_T *xC,
			const real_T  *params, const int_T  p_width0)
{
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_BEGIN --- EDIT HERE TO _END */
uint8_T i;

for( i=0;i<NOF_STATES;i++)
{
    outStates[i] = xC[i];
}
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_END --- EDIT HERE TO _BEGIN */
}

/*
  *  Derivatives function
  *
  */
void vehicleslrt_Derivatives_wrapper(const real_T *inSteer,
			const real_T *inMz,
			const real_T *outStates,
			real_T *dx,
			real_T *xC,
			const real_T  *params,  const int_T  p_width0)
{
/* %%%-SFUNWIZ_wrapper_Derivatives_Changes_BEGIN --- EDIT HERE TO _END */
const real_T aux_Mz_lat[4]= {A,A,-B,-B};
const real_T aux_Mz_long[4] = {-TF2,TF2,-TR2,TR2};
const real_T aux_slip_angle_num[4] = {A,A,-B,-B};
const real_T aux_slip_angle_den[4] = {-TF2,TF2,-TR2,TR2};
const real_T aux_load_f1[4] = {ZF0,ZF0,ZR0,ZR0};
const real_T aux_load_f2[4] = {-Z[1],Z[1],-Z[6],Z[6]};
const real_T aux_load_f3[4] = {-Z[0],-Z[0],Z[0],Z[0]};
const real_T aux_load_f4[4] = {-Z[2],Z[2],-Z[4],Z[4]};
const real_T aux_load_f5[4] = {-Z[3],Z[3],-Z[5],Z[5]};
const real_T aux_wspeed[4] = {-B/2,B/2,-B/2,B/2};
static bool isFirstIteration = 1;
uint8_T i,j;
real_T aux_cos_steer,aux_sin_steer,aux_cos_rsteer,aux_sin_rsteer;
real_T veh_long_f[4], veh_lat_f[4];
real_T Mz,Fy,Fx;
real_T loadKN[4];
real_T mf_D,mf_E,mf_Bs;
real_T slip_angle[4] ={0,0,0,0};
real_T lat_f[4] ={0,0,0,0};
real_T load[4] ={0,0,0,0};
real_T sh,sv,camber;
aux_cos_steer = cos(inSteer[0]+FRONT_STEER_BY_ROLL*ROLL);
aux_sin_steer = sin(inSteer[0]+FRONT_STEER_BY_ROLL*ROLL);
aux_cos_rsteer = cos(REAR_STEER_BY_ROLL*ROLL);
aux_sin_rsteer = sin(REAR_STEER_BY_ROLL*ROLL);
Mz = 0;
Fy = 0; 
Fx = 0;
if( isFirstIteration )
{
    isFirstIteration = 0;
    ssPrintf("\n aceleracao inicial: %f , %f", LAT_ACCEL, LONG_ACCEL );
    for(j=0;j<=40;j++) 
    {
        ssPrintf("\n param %d %f", j, params[j]);
    }

}

for( i=FL; i<= RR; i++ )
{

    slip_angle[i] = 0;
    if( ( i == FL) || ( i == FR) )
    {
        slip_angle[i] = inSteer[0]; 
    }
    
    slip_angle[i] -= atan2(LAST_V+LAST_YAW_RATE*aux_slip_angle_num[i],CONST_SPEED+LAST_YAW_RATE*aux_slip_angle_den[i]);
    
    load[i] = aux_load_f1[i] +
              aux_load_f2[i]*LAT_ACCEL +
              aux_load_f3[i]*LONG_ACCEL +
              aux_load_f4[i]*LAST_ROLL +
              aux_load_f5[i]*LAST_ROLL_RATE;
    camber = CAMBER_BY_ROLL*LAST_ROLL;
    loadKN[i] = load[i]*0.001;
    sh = (MFA[8]*camber+MFA[9]*loadKN[i]+MFA[10])*M_PI/180;
    sv = (MFA[11]*loadKN[i]*loadKN[i]+MFA[12]*loadKN[i])*camber+MFA[13]*loadKN[i]+MFA[14];
    mf_D = FRICTION*(MFA[1]*loadKN[i] + MFA[2])*loadKN[i];
    mf_E = MFA[6]*loadKN[i] + MFA[7];
    mf_Bs = (MFA[3]*(180/M_PI)*sin(2*atan(loadKN[i]/MFA[4]))/(MFA[0]*mf_D))*(1-MFA[5]*fabs(camber))*(slip_angle[i]+sh);
    lat_f[i] = mf_D*sin(MFA[0]*atan((1-mf_E)*mf_Bs + mf_E*atan(mf_Bs))+sv);      
}

for( i=FL; i<= RR; i++ )
{    
     if( ( i == FL) || ( i == FR) )
     {
         veh_long_f[i] = -lat_f[i]*aux_sin_steer;
         veh_lat_f[i]  = lat_f[i]*aux_cos_steer; 
     }
     else
     {
         veh_long_f[i] = -lat_f[i]*aux_sin_rsteer;
         veh_lat_f[i] = lat_f[i]*aux_cos_rsteer;                 
     }
    
    Mz += aux_Mz_lat[i]*veh_lat_f[i]+ aux_Mz_long[i]*veh_long_f[i];
    Fy += veh_lat_f[i];
}

Mz += inMz[0];

dx[ROLL_ID] = ROLL_RATE;
dx[ROLL_RATE_ID] = D[0]*Fy + D[1]*Mz + D[2]*sin(ROLL) - D[3]*ROLL - D[4]*ROLL_RATE;
dx[YAW_RATE_ID] = D[5]*Mz - D[6]*dx[ROLL_RATE_ID];
dx[V_ID] = D[10]*Fy + D[11]*dx[ROLL_RATE_ID] - D[12]*YAW_RATE*CONST_SPEED;

//dx[STEER_RATE_ID] = (-KSS*(STEER-inSteer[0]) + STEER_BY_ROLL*KSS*ROLL +
//        H_STEER*STEER_RATE - XC*(lat_f[0]+lat_f[1]) - YAW_RATE)/IFW;
//dx[STEER_ID] = STEER_RATE;
/* %%%-SFUNWIZ_wrapper_Derivatives_Changes_END --- EDIT HERE TO _BEGIN */
}
