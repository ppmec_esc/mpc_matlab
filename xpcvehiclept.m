function pt=xpcvehiclept
pt = [];
pt(1).blockname = 'Estados lidos e comando enviado  pelo controlador/ Leitura do comando';
pt(1).paramname = 'P1';
pt(1).class     = 'vector';
pt(1).nrows     = 1;
pt(1).ncols     = 11;
pt(1).subsource = 'SS_DOUBLE';
pt(1).ndims     = '2';
pt(1).size      = '[]';
pt(getlenPT) = pt(1);

pt(2).blockname = 'Estados lidos e comando enviado  pelo controlador/ Leitura do comando';
pt(2).paramname = 'P2';
pt(2).class     = 'scalar';
pt(2).nrows     = 1;
pt(2).ncols     = 1;
pt(2).subsource = 'SS_DOUBLE';
pt(2).ndims     = '2';
pt(2).size      = '[]';

pt(3).blockname = 'Estados lidos e comando enviado  pelo controlador/ Leitura do comando';
pt(3).paramname = 'P3';
pt(3).class     = 'scalar';
pt(3).nrows     = 1;
pt(3).ncols     = 1;
pt(3).subsource = 'SS_DOUBLE';
pt(3).ndims     = '2';
pt(3).size      = '[]';

pt(4).blockname = 'Estados lidos e comando enviado  pelo controlador/ Leitura do comando';
pt(4).paramname = 'P4';
pt(4).class     = 'scalar';
pt(4).nrows     = 1;
pt(4).ncols     = 1;
pt(4).subsource = 'SS_DOUBLE';
pt(4).ndims     = '2';
pt(4).size      = '[]';

pt(5).blockname = 'Estados lidos e comando enviado  pelo controlador/ Leitura do comando';
pt(5).paramname = 'P5';
pt(5).class     = 'scalar';
pt(5).nrows     = 1;
pt(5).ncols     = 1;
pt(5).subsource = 'SS_DOUBLE';
pt(5).ndims     = '2';
pt(5).size      = '[]';

pt(6).blockname = 'PCI-DAC6703/Saturation1';
pt(6).paramname = 'UpperLimit';
pt(6).class     = 'scalar';
pt(6).nrows     = 1;
pt(6).ncols     = 1;
pt(6).subsource = 'SS_DOUBLE';
pt(6).ndims     = '2';
pt(6).size      = '[]';

pt(7).blockname = 'PCI-DAC6703/Saturation1';
pt(7).paramname = 'LowerLimit';
pt(7).class     = 'scalar';
pt(7).nrows     = 1;
pt(7).ncols     = 1;
pt(7).subsource = 'SS_DOUBLE';
pt(7).ndims     = '2';
pt(7).size      = '[]';

pt(8).blockname = 'PCI-DAC6703/Saturation2';
pt(8).paramname = 'UpperLimit';
pt(8).class     = 'scalar';
pt(8).nrows     = 1;
pt(8).ncols     = 1;
pt(8).subsource = 'SS_DOUBLE';
pt(8).ndims     = '2';
pt(8).size      = '[]';

pt(9).blockname = 'PCI-DAC6703/Saturation2';
pt(9).paramname = 'LowerLimit';
pt(9).class     = 'scalar';
pt(9).nrows     = 1;
pt(9).ncols     = 1;
pt(9).subsource = 'SS_DOUBLE';
pt(9).ndims     = '2';
pt(9).size      = '[]';

pt(10).blockname = 'PCI-DAC6703/Saturation3';
pt(10).paramname = 'UpperLimit';
pt(10).class     = 'scalar';
pt(10).nrows     = 1;
pt(10).ncols     = 1;
pt(10).subsource = 'SS_DOUBLE';
pt(10).ndims     = '2';
pt(10).size      = '[]';

pt(11).blockname = 'PCI-DAC6703/Saturation3';
pt(11).paramname = 'LowerLimit';
pt(11).class     = 'scalar';
pt(11).nrows     = 1;
pt(11).ncols     = 1;
pt(11).subsource = 'SS_DOUBLE';
pt(11).ndims     = '2';
pt(11).size      = '[]';

pt(12).blockname = 'PCI-DAC6703/Saturation4';
pt(12).paramname = 'UpperLimit';
pt(12).class     = 'scalar';
pt(12).nrows     = 1;
pt(12).ncols     = 1;
pt(12).subsource = 'SS_DOUBLE';
pt(12).ndims     = '2';
pt(12).size      = '[]';

pt(13).blockname = 'PCI-DAC6703/Saturation4';
pt(13).paramname = 'LowerLimit';
pt(13).class     = 'scalar';
pt(13).nrows     = 1;
pt(13).ncols     = 1;
pt(13).subsource = 'SS_DOUBLE';
pt(13).ndims     = '2';
pt(13).size      = '[]';

pt(14).blockname = 'PCI-DAC6703/Saturation5';
pt(14).paramname = 'UpperLimit';
pt(14).class     = 'scalar';
pt(14).nrows     = 1;
pt(14).ncols     = 1;
pt(14).subsource = 'SS_DOUBLE';
pt(14).ndims     = '2';
pt(14).size      = '[]';

pt(15).blockname = 'PCI-DAC6703/Saturation5';
pt(15).paramname = 'LowerLimit';
pt(15).class     = 'scalar';
pt(15).nrows     = 1;
pt(15).ncols     = 1;
pt(15).subsource = 'SS_DOUBLE';
pt(15).ndims     = '2';
pt(15).size      = '[]';

pt(16).blockname = 'PCI-DAC6703/PCI-DAC6703 DA2';
pt(16).paramname = 'P1';
pt(16).class     = 'vector';
pt(16).nrows     = 1;
pt(16).ncols     = 5;
pt(16).subsource = 'SS_DOUBLE';
pt(16).ndims     = '2';
pt(16).size      = '[]';

pt(17).blockname = 'PCI-DAC6703/PCI-DAC6703 DA2';
pt(17).paramname = 'P2';
pt(17).class     = 'vector';
pt(17).nrows     = 1;
pt(17).ncols     = 5;
pt(17).subsource = 'SS_DOUBLE';
pt(17).ndims     = '2';
pt(17).size      = '[]';

pt(18).blockname = 'PCI-DAC6703/PCI-DAC6703 DA2';
pt(18).paramname = 'P3';
pt(18).class     = 'vector';
pt(18).nrows     = 1;
pt(18).ncols     = 5;
pt(18).subsource = 'SS_DOUBLE';
pt(18).ndims     = '2';
pt(18).size      = '[]';

pt(19).blockname = 'PCI-DAC6703/PCI-DAC6703 DA2';
pt(19).paramname = 'P4';
pt(19).class     = 'scalar';
pt(19).nrows     = 1;
pt(19).ncols     = 1;
pt(19).subsource = 'SS_DOUBLE';
pt(19).ndims     = '2';
pt(19).size      = '[]';

pt(20).blockname = 'PCI-DAC6703/PCI-DAC6703 DA2';
pt(20).paramname = 'P5';
pt(20).class     = 'scalar';
pt(20).nrows     = 1;
pt(20).ncols     = 1;
pt(20).subsource = 'SS_DOUBLE';
pt(20).ndims     = '2';
pt(20).size      = '[]';

pt(21).blockname = 'PCI-DAC6703/PCI-DAC6703 DA2';
pt(21).paramname = 'P6';
pt(21).class     = 'scalar';
pt(21).nrows     = 1;
pt(21).ncols     = 1;
pt(21).subsource = 'SS_DOUBLE';
pt(21).ndims     = '2';
pt(21).size      = '[]';

pt(22).blockname = 'Simula��o dos  movimentos do ve�culo/Memory';
pt(22).paramname = 'X0';
pt(22).class     = 'vector';
pt(22).nrows     = 1;
pt(22).ncols     = 4;
pt(22).subsource = 'SS_DOUBLE';
pt(22).ndims     = '2';
pt(22).size      = '[]';

pt(23).blockname = 'Simula��o dos  movimentos do ve�culo/Rate Transition';
pt(23).paramname = 'X0';
pt(23).class     = 'scalar';
pt(23).nrows     = 1;
pt(23).ncols     = 1;
pt(23).subsource = 'SS_DOUBLE';
pt(23).ndims     = '2';
pt(23).size      = '[]';

pt(24).blockname = 'Simula��o dos  movimentos do ve�culo/Rate Limiter';
pt(24).paramname = 'RisingSlewLimit';
pt(24).class     = 'scalar';
pt(24).nrows     = 1;
pt(24).ncols     = 1;
pt(24).subsource = 'SS_DOUBLE';
pt(24).ndims     = '2';
pt(24).size      = '[]';

pt(25).blockname = 'Simula��o dos  movimentos do ve�culo/Rate Limiter';
pt(25).paramname = 'FallingSlewLimit';
pt(25).class     = 'scalar';
pt(25).nrows     = 1;
pt(25).ncols     = 1;
pt(25).subsource = 'SS_DOUBLE';
pt(25).ndims     = '2';
pt(25).size      = '[]';

pt(26).blockname = 'Simula��o dos  movimentos do ve�culo/Rate Limiter';
pt(26).paramname = 'InitialCondition';
pt(26).class     = 'scalar';
pt(26).nrows     = 1;
pt(26).ncols     = 1;
pt(26).subsource = 'SS_DOUBLE';
pt(26).ndims     = '2';
pt(26).size      = '[]';

pt(27).blockname = 'Simula��o dos  movimentos do ve�culo/Saturation';
pt(27).paramname = 'UpperLimit';
pt(27).class     = 'scalar';
pt(27).nrows     = 1;
pt(27).ncols     = 1;
pt(27).subsource = 'SS_DOUBLE';
pt(27).ndims     = '2';
pt(27).size      = '[]';

pt(28).blockname = 'Simula��o dos  movimentos do ve�culo/Saturation';
pt(28).paramname = 'LowerLimit';
pt(28).class     = 'scalar';
pt(28).nrows     = 1;
pt(28).ncols     = 1;
pt(28).subsource = 'SS_DOUBLE';
pt(28).ndims     = '2';
pt(28).size      = '[]';

pt(29).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Integrator';
pt(29).paramname = 'InitialCondition';
pt(29).class     = 'scalar';
pt(29).nrows     = 1;
pt(29).ncols     = 1;
pt(29).subsource = 'SS_DOUBLE';
pt(29).ndims     = '2';
pt(29).size      = '[]';

pt(30).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Integrator1';
pt(30).paramname = 'InitialCondition';
pt(30).class     = 'scalar';
pt(30).nrows     = 1;
pt(30).ncols     = 1;
pt(30).subsource = 'SS_DOUBLE';
pt(30).ndims     = '2';
pt(30).size      = '[]';

pt(31).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Integrator2';
pt(31).paramname = 'InitialCondition';
pt(31).class     = 'scalar';
pt(31).nrows     = 1;
pt(31).ncols     = 1;
pt(31).subsource = 'SS_DOUBLE';
pt(31).ndims     = '2';
pt(31).size      = '[]';

pt(32).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Saturation';
pt(32).paramname = 'UpperLimit';
pt(32).class     = 'scalar';
pt(32).nrows     = 1;
pt(32).ncols     = 1;
pt(32).subsource = 'SS_DOUBLE';
pt(32).ndims     = '2';
pt(32).size      = '[]';

pt(33).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/Saturation';
pt(33).paramname = 'LowerLimit';
pt(33).class     = 'scalar';
pt(33).nrows     = 1;
pt(33).ncols     = 1;
pt(33).subsource = 'SS_DOUBLE';
pt(33).ndims     = '2';
pt(33).size      = '[]';

pt(34).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System/Internal';
pt(34).paramname = 'D';
pt(34).class     = 'scalar';
pt(34).nrows     = 1;
pt(34).ncols     = 1;
pt(34).subsource = 'SS_DOUBLE';
pt(34).ndims     = '2';
pt(34).size      = '[]';

pt(35).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System/Internal';
pt(35).paramname = 'X0';
pt(35).class     = 'scalar';
pt(35).nrows     = 1;
pt(35).ncols     = 1;
pt(35).subsource = 'SS_DOUBLE';
pt(35).ndims     = '2';
pt(35).size      = '[]';

pt(36).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System1/Internal';
pt(36).paramname = 'B';
pt(36).class     = 'scalar';
pt(36).nrows     = 1;
pt(36).ncols     = 1;
pt(36).subsource = 'SS_DOUBLE';
pt(36).ndims     = '2';
pt(36).size      = '[]';

pt(37).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System1/Internal';
pt(37).paramname = 'C';
pt(37).class     = 'scalar';
pt(37).nrows     = 1;
pt(37).ncols     = 1;
pt(37).subsource = 'SS_DOUBLE';
pt(37).ndims     = '2';
pt(37).size      = '[]';

pt(38).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System1/Internal';
pt(38).paramname = 'X0';
pt(38).class     = 'scalar';
pt(38).nrows     = 1;
pt(38).ncols     = 1;
pt(38).subsource = 'SS_DOUBLE';
pt(38).ndims     = '2';
pt(38).size      = '[]';

pt(39).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System2/Internal';
pt(39).paramname = 'D';
pt(39).class     = 'scalar';
pt(39).nrows     = 1;
pt(39).ncols     = 1;
pt(39).subsource = 'SS_DOUBLE';
pt(39).ndims     = '2';
pt(39).size      = '[]';

pt(40).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System2/Internal';
pt(40).paramname = 'X0';
pt(40).class     = 'scalar';
pt(40).nrows     = 1;
pt(40).ncols     = 1;
pt(40).subsource = 'SS_DOUBLE';
pt(40).ndims     = '2';
pt(40).size      = '[]';

pt(41).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System3/Internal';
pt(41).paramname = 'D';
pt(41).class     = 'scalar';
pt(41).nrows     = 1;
pt(41).ncols     = 1;
pt(41).subsource = 'SS_DOUBLE';
pt(41).ndims     = '2';
pt(41).size      = '[]';

pt(42).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System3/Internal';
pt(42).paramname = 'X0';
pt(42).class     = 'scalar';
pt(42).nrows     = 1;
pt(42).ncols     = 1;
pt(42).subsource = 'SS_DOUBLE';
pt(42).ndims     = '2';
pt(42).size      = '[]';

pt(43).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System4/Internal';
pt(43).paramname = 'D';
pt(43).class     = 'scalar';
pt(43).nrows     = 1;
pt(43).ncols     = 1;
pt(43).subsource = 'SS_DOUBLE';
pt(43).ndims     = '2';
pt(43).size      = '[]';

pt(44).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System4/Internal';
pt(44).paramname = 'X0';
pt(44).class     = 'scalar';
pt(44).nrows     = 1;
pt(44).ncols     = 1;
pt(44).subsource = 'SS_DOUBLE';
pt(44).ndims     = '2';
pt(44).size      = '[]';

pt(45).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System/Input Delay/Transport Delay';
pt(45).paramname = 'DelayTime';
pt(45).class     = 'scalar';
pt(45).nrows     = 1;
pt(45).ncols     = 1;
pt(45).subsource = 'SS_DOUBLE';
pt(45).ndims     = '2';
pt(45).size      = '[]';

pt(46).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System/Input Delay/Transport Delay';
pt(46).paramname = 'InitialOutput';
pt(46).class     = 'scalar';
pt(46).nrows     = 1;
pt(46).ncols     = 1;
pt(46).subsource = 'SS_DOUBLE';
pt(46).ndims     = '2';
pt(46).size      = '[]';

pt(47).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System1/Input Delay/Transport Delay';
pt(47).paramname = 'DelayTime';
pt(47).class     = 'scalar';
pt(47).nrows     = 1;
pt(47).ncols     = 1;
pt(47).subsource = 'SS_DOUBLE';
pt(47).ndims     = '2';
pt(47).size      = '[]';

pt(48).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System1/Input Delay/Transport Delay';
pt(48).paramname = 'InitialOutput';
pt(48).class     = 'scalar';
pt(48).nrows     = 1;
pt(48).ncols     = 1;
pt(48).subsource = 'SS_DOUBLE';
pt(48).ndims     = '2';
pt(48).size      = '[]';

pt(49).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System2/Input Delay/Transport Delay';
pt(49).paramname = 'DelayTime';
pt(49).class     = 'scalar';
pt(49).nrows     = 1;
pt(49).ncols     = 1;
pt(49).subsource = 'SS_DOUBLE';
pt(49).ndims     = '2';
pt(49).size      = '[]';

pt(50).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System2/Input Delay/Transport Delay';
pt(50).paramname = 'InitialOutput';
pt(50).class     = 'scalar';
pt(50).nrows     = 1;
pt(50).ncols     = 1;
pt(50).subsource = 'SS_DOUBLE';
pt(50).ndims     = '2';
pt(50).size      = '[]';

pt(51).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System3/Input Delay/Transport Delay';
pt(51).paramname = 'DelayTime';
pt(51).class     = 'scalar';
pt(51).nrows     = 1;
pt(51).ncols     = 1;
pt(51).subsource = 'SS_DOUBLE';
pt(51).ndims     = '2';
pt(51).size      = '[]';

pt(52).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System3/Input Delay/Transport Delay';
pt(52).paramname = 'InitialOutput';
pt(52).class     = 'scalar';
pt(52).nrows     = 1;
pt(52).ncols     = 1;
pt(52).subsource = 'SS_DOUBLE';
pt(52).ndims     = '2';
pt(52).size      = '[]';

pt(53).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System4/Input Delay/Transport Delay';
pt(53).paramname = 'DelayTime';
pt(53).class     = 'scalar';
pt(53).nrows     = 1;
pt(53).ncols     = 1;
pt(53).subsource = 'SS_DOUBLE';
pt(53).ndims     = '2';
pt(53).size      = '[]';

pt(54).blockname = 'Simula��o dos  movimentos do ve�culo/AndrzejDriver1/LTI System4/Input Delay/Transport Delay';
pt(54).paramname = 'InitialOutput';
pt(54).class     = 'scalar';
pt(54).nrows     = 1;
pt(54).ncols     = 1;
pt(54).subsource = 'SS_DOUBLE';
pt(54).ndims     = '2';
pt(54).size      = '[]';

pt(55).blockname = '';
pt(55).paramname = 'VY_GAIN';
pt(55).class     = 'scalar';
pt(55).nrows     = 1;
pt(55).ncols     = 1;
pt(55).subsource = 'SS_DOUBLE';
pt(55).ndims     = '2';
pt(55).size      = '[]';

pt(56).blockname = '';
pt(56).paramname = 'model_long_speed';
pt(56).class     = 'scalar';
pt(56).nrows     = 1;
pt(56).ncols     = 1;
pt(56).subsource = 'SS_DOUBLE';
pt(56).ndims     = '2';
pt(56).size      = '[]';

pt(57).blockname = '';
pt(57).paramname = 'params';
pt(57).class     = 'vector';
pt(57).nrows     = 1;
pt(57).ncols     = 65;
pt(57).subsource = 'SS_DOUBLE';
pt(57).ndims     = '2';
pt(57).size      = '[]';

pt(58).blockname = '';
pt(58).paramname = 'path';
pt(58).class     = 'col-mat';
pt(58).nrows     = 10000;
pt(58).ncols     = 2;
pt(58).subsource = 'SS_DOUBLE';
pt(58).ndims     = '2';
pt(58).size      = '[]';

pt(59).blockname = '';
pt(59).paramname = 'path_lb';
pt(59).class     = 'vector';
pt(59).nrows     = 10000;
pt(59).ncols     = 1;
pt(59).subsource = 'SS_DOUBLE';
pt(59).ndims     = '2';
pt(59).size      = '[]';

pt(60).blockname = '';
pt(60).paramname = 'path_ub';
pt(60).class     = 'vector';
pt(60).nrows     = 10000;
pt(60).ncols     = 1;
pt(60).subsource = 'SS_DOUBLE';
pt(60).ndims     = '2';
pt(60).size      = '[]';

function len = getlenPT
len = 60;

