function driver = driver_settings(W,La,Tk,long_speed)
    Tp = (La/long_speed) - Tk;
    s=zpk('s');
    driver.PREVIEWLOG = tf([1],[1],'InputDelay',Tp);
    driver.VY_GAIN = W/La;
    driver.VY_FB = tf([W/La],[1 0],'InputDelay',Tk);
    driver.YAW_FB = tf([W],[1],'InputDelay',Tk);
end
