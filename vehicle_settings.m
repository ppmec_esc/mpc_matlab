    function out = vehicle_settings(...
    a,... %dist�ncia entre cg e eixo frontal
    b,... %dist�ncia entre cg e eixo traseiropl
    t_f,... %comprimento do eixo frontal
    t_r,... %comprimento do eixo traseiro
    hg,... %altura do cg
    m,... %massa
    hs,... %altura do centro de rolagem
    ms,... %massaa sobre centro de rolagem
    kf,... %coeficiente de rigidez frontal a rolagem
    kr,... %coeficiente de rigidez traseira a rolagem
    cf,... %coeficiente de amortecimento frontal a rolagem
    cr,... %coeficiente de amortecimental traseiro a rolagem
    roll_damping,... %coeficiente de rigidez a rolagem
    roll_stiffness,... %coeficiente de amortecimento a rolagem
    Izz,... %momento de in�rcia do eixo de guinada
    Ixx,... %momento de in�rcia do eixo de rolagem
    Ixz,... %produto de in�rcia dos eixos de rolagem e guinada
    g,... %acelera��o gravitacional
    L,... %dist�ncia entre os eixos frontal e traseiro       
    MFA,... %coefficients of magic formula for lateral force
    MFB,... %coefficients of magic formula for longitudinal force
    Reff,... % wheel effective radio
    Iw,... % wheel inertial moment
    camber_by_roll, ... % camber by roll ratio
    friction, ... % tire-road friction coefficients
    long_speed, ... % initial longitudinal speed
    Ifw,...
    Kss,...
    steer_by_roll,...
    xc,...
    h_steer,...
    front_steer_grad,...
    rear_steer_grad ...
)
    Fze = ([b,b,a,a]*m*g/(200*L));
    
    eta = m*Ixx*Izz - m*Ixz*Ixz - ms*ms*hs*hs*Izz;

    d(1) = ms*hs*Izz;
    d(2) = m*Ixz;
    d(3) = m*Izz*ms*hs*g;
    d(4) = m*Izz*roll_stiffness;
    d(5) = m*Izz*roll_damping;
    d(1:5) = d(1:5)/eta;
    d(6) = 1/Izz;
    d(7) = Ixz/Izz;
    d(8) = 1/m;
    d(9) = 1;
    d(10) = ms*hs/m;
    d(11) = 1/m;
    d(12) = ms*hs/m;
    d(13) = 1;
    d(14) = 1/Iw;
    d(15) = Reff/Iw;
    
    zf0 = m*g*b/(2*L);
    zr0 = m*g*a/(2*L);
    z(1) = m*hg*b/(2*L);
    z(2) = m*hg/(L*t_f);
    z(3) = kf/t_f;
    z(4) = cf/t_f;
    z(5) = kr/t_r;
    z(6) = cr/t_r;
    z(7) = m*hg*a/(2*L);
    tf2 = t_f/2;
    tr2 = t_r/2;
    
    out.params =... 
    	[MFA,MFB,z,zf0,zr0,a,b,tf2,tr2,d,Reff,friction,long_speed,camber_by_roll,...
        Ifw,Kss,steer_by_roll,xc,h_steer,front_steer_grad,rear_steer_grad];

    out.a = a;
    out.b = b;
    out.t_f = t_f;
    out.t_r = t_r;
    out.hg = hg;
    out.m = m;
    out.hs = hs;
    out.ms = ms;
    out.kf = kf;
    out.kr = kr;
    out.cf = cf;
    out.cr = cr;
    out.roll_damping = roll_damping;
    out.roll_stiffness = roll_stiffness;
    out.Izz = Izz;
    out.Ixx = Ixx;
    out.Ixz = Ixz;
    out.g = g;
    out.L = L;
    out.MFA = MFA;
    out.MFB = MFB;
    out.Reff = Reff;
    out.Iw = Iw;
    out.camber_by_roll = camber_by_roll;
    out.friction = friction;
    out.long_speed = long_speed;
    out.Ifw = Ifw;
    out.Kss = Kss;
    out.steer_by_roll = steer_by_roll;
    out.xc = xc;
    out.h_steer = h_steer;
    out.load = [zf0 zf0 zr0 zr0];
    
end
