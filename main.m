%% Configuraçao

% It just selects which vars set will be used in this simulation.
% Edit simmsettigs.m to enable/disable roll control
roll_enabled = true;
if roll_enabled == true
    vars = 1.0e+03 *[5.717202948455769 0.680142902721523 0.325226457102194 1.742253305714026];
else
    vars = 1.0e+03 *[ 6.632028644491468   0.583662601088253   1.999632252422914]
end

% Selects the maneuver speed
speed = 100/3.6;

% Load the simulation settings: vehicle, driver, maneuver, and
% controller.
simsettings;
  
% Run simulation.
simresult = domaneuver(veh_model, driver_model, control_model, maneuver, sample_time, open_loop, sim_time, prof_enabled);
   
% Show results.
plotpath(simresult,3,open_loop)    
plotnonlinearresult