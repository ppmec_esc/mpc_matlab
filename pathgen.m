function path = pathgen(maneuver,nof_pts,max_x,t_r)
% function path = pathgen(maneuver,nof_pts,max_x,t_r)
% maneuver is a string with maneuver name, supported : 'doublelane'
% nof_pts is the number of points in path
% max_x is the maximum longitudinal displacement
% t_r is the vehicle`s rear track width
   switch maneuver 
       case 'doublelane'
           maxy = (3.5 - ((1.1*t_r + 0.25)/2)) + (1.2*t_r+0.25)/2;
           th1 = (1.1*t_r+0.25)/2;
           th2 = (1.2*t_r+0.25)/2;
           th3 = (1.3*t_r+0.25)/2;
           path1 = 15;
           path2 = path1 + 30;
           path3 = path2 + 25;
           path4 = path3 + 25;
           path5 = path4 + 30;
           
           pathx = max_x*([0:(nof_pts-1)])/nof_pts;
           y = pathx*0;
           lb = y;
           ub = y;
           
           i = 1;%�floor((path1-15)*nof_pts/max_x);
           f = floor((path2+10)*nof_pts/max_x);
           x = ([i:f] - ((i+f)/2))/(f-(i+f)/2);
           sigpath = 1./(1+exp(-8*x));
           sigpath = sigpath - min(sigpath);
           sigpath = sigpath/max(sigpath);
           y(i:f) = sigpath;
           i = f;
           f = floor((path3-15)*nof_pts/max_x);
           y(i:f) = 1;
           i = f;
           f = floor((path4+15)*nof_pts/max_x);
           x = ([i:f] - ((i+f)/2))/(f-(i+f)/2);
           x = -x;
           sigpath = 1./(1+exp(-8*x));
           sigpath = sigpath - min(sigpath);
           sigpath = sigpath/max(sigpath);
           y(i:f) = sigpath;
           i = f;
           f = floor(path5*nof_pts/max_x);
           y(i:f) = 0;
           y = y*maxy;
           
           i = 1;
           f = floor(path2*nof_pts/max_x);
           lb(i:f)= - th1;
           
           i = f;
           f = floor(path3*nof_pts/max_x);
           lb(i:f)=3.5 - th1;
           
           i = f;
           lb(i:end) = - th3;
           f = floor(path1*nof_pts/max_x);
           i = 1;
           f = floor(path1*nof_pts/max_x);
           ub(i:f) = th1;
           
           i=f;
           f = floor(path4*nof_pts/max_x);
           ub(i:f) = 3.5 + 2*th2 - th1;
           
           i=f;
           ub(i:end) = th3;
           
           
           
           path(:,1) = pathx;%dlcpath(:,1)*max_x/dlcpath(end,1);
           path(:,2) = y;%lcpath(:,2)*maxy/max(dlcpath(:,2));
           path(:,3) = lb;
           path(:,4) = ub;
       case 'jturn'
           r = 60;
           s = 100;
           z = 20;
           q = 100;
           cx = z+s+r*cos(pi/3);
           ys = -r*sin(pi/3);
           xf= q+r*cosd(15)+cx;
           dx = xf/(nof_pts-1);
           x = 0:dx:xf;
           zind = ceil(z/dx)+1;
           y = 0*x;
           s_ind = ceil((z+s)/dx)+1;
           y(zind+1:s_ind) = ys-tan(pi/6)*(x(zind+1:s_ind)-z-s);
           rind = ceil((cx+r*cosd(15))/dx)+1;
           y(s_ind+1:rind) = -sqrt(r*r-(x(s_ind+1:rind)-cx).^2);
           y(rind+1:length(x) )= tand(75)*(x(rind+1:end)-y(rind))-r*sind(5);
           
           y = y - y(zind+1);
           y(1:zind) = 0;
           %plot(x,y)
           path(:,1) = x;%dlcpath(:,1)*max_x/dlcpath(end,1);
           path(:,2) = y;%lcpath(:,2)*maxy/max(dlcpath(:,2));
           path(:,3) = y;
           path(:,4) = y;
           

end
