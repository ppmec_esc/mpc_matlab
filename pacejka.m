MFB = [1.57 ,-48   , 1338 ,   5.8   , 444  , 0  , 0.003, -0.008,0.66,0, 0];
camber = 0;

loadKN = veh_model.load(1)/1000;

D = (MFB(2)*loadKN + MFB(3))*loadKN;
E = (MFB(7)*loadKN+MFB(8))*loadKN + MFB(9);

long_f = [];
long_slip = -0.01:0.02:0.01;
for i = 1:length(long_slip)
    Bs = ((MFB(4)*loadKN + MFB(5))*loadKN)/(MFB(1)*D*exp(MFB(6)*loadKN))*long_slip(i)*180/pi;
    long_f = [long_f D*sin(MFB(1)*atan((1-E)*Bs + E*atan(Bs)))];    
end

C_lambda = (long_f(2)-long_f(1))/0.02;

long_f = [];
long_slip = -0.1:0.0001:0.1;
for i = 1:length(long_slip)
    Bs = ((MFB(4)*loadKN + MFB(5))*loadKN)/(MFB(1)*D*exp(MFB(6)*loadKN))*long_slip(i)*180/pi;
    long_f = [long_f D*sin(MFB(1)*atan((1-E)*Bs + E*atan(Bs)))];    
end

plot(long_slip,long_f)
hold on
plot(long_slip, C_lambda*long_slip);

x = abs(C_lambda*long_slip - long_f)./long_f;
f = find(abs(x)<0.05);

plot(long_slip(f),long_f(f))
max_long_f = max(long_f(f));

