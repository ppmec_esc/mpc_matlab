/*
 * _coder_solver_run_info.h
 *
 * Code generation for function 'solver_run'
 *
 */

#ifndef ___CODER_SOLVER_RUN_INFO_H__
#define ___CODER_SOLVER_RUN_INFO_H__
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);

#endif
/* End of code generation (_coder_solver_run_info.h) */
