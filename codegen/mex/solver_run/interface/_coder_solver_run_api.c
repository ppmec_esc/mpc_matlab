/*
 * _coder_solver_run_api.c
 *
 * Code generation for function '_coder_solver_run_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "solver_run.h"
#include "_coder_solver_run_api.h"
#include "solver_run_data.h"

/* Function Declarations */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct0_T *y);
static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[5]);
static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *arg_H,
  const char_T *identifier))[25];
static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *obj, const
  char_T *identifier, struct0_T *y);
static const mxArray *emlrt_marshallOut(const real_T u[5]);
static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[25];
static real_T (*g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *arg_F,
  const char_T *identifier))[5];
static real_T (*h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[5];
static real_T (*i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *arg_A,
  const char_T *identifier))[1050];
static real_T (*j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[1050];
static real_T (*k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *arg_B,
  const char_T *identifier))[210];
static real_T (*l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[210];
static real_T m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);
static void n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[5]);
static real_T (*o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[25];
static real_T (*p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[5];
static real_T (*q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[1050];
static real_T (*r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[210];

/* Function Definitions */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct0_T *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[9] = { "rho0", "rho_max", "N_iter", "p0",
    "gam_min", "beta_plus", "beta_minus", "n_rho", "rho" };

  thisId.fParent = parentId;
  emlrtCheckStructR2012b(sp, parentId, u, 9, fieldNames, 0U, 0);
  thisId.fIdentifier = "rho0";
  y->rho0 = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0,
    "rho0")), &thisId);
  thisId.fIdentifier = "rho_max";
  y->rho_max = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0,
    "rho_max")), &thisId);
  thisId.fIdentifier = "N_iter";
  y->N_iter = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0,
    "N_iter")), &thisId);
  thisId.fIdentifier = "p0";
  d_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0, "p0")),
                     &thisId, y->p0);
  thisId.fIdentifier = "gam_min";
  y->gam_min = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0,
    "gam_min")), &thisId);
  thisId.fIdentifier = "beta_plus";
  y->beta_plus = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0,
    "beta_plus")), &thisId);
  thisId.fIdentifier = "beta_minus";
  y->beta_minus = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0,
    "beta_minus")), &thisId);
  thisId.fIdentifier = "n_rho";
  y->n_rho = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0,
    "n_rho")), &thisId);
  thisId.fIdentifier = "rho";
  y->rho = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2013a(sp, u, 0, "rho")),
    &thisId);
  emlrtDestroyArray(&u);
}

static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = m_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[5])
{
  n_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *arg_H,
  const char_T *identifier))[25]
{
  real_T (*y)[25];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = f_emlrt_marshallIn(sp, emlrtAlias(arg_H), &thisId);
  emlrtDestroyArray(&arg_H);
  return y;
}
  static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *obj, const
  char_T *identifier, struct0_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  b_emlrt_marshallIn(sp, emlrtAlias(obj), &thisId, y);
  emlrtDestroyArray(&obj);
}

static const mxArray *emlrt_marshallOut(const real_T u[5])
{
  const mxArray *y;
  static const int32_T iv0[1] = { 0 };

  const mxArray *m0;
  static const int32_T iv1[1] = { 5 };

  y = NULL;
  m0 = emlrtCreateNumericArray(1, iv0, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m0, (void *)u);
  emlrtSetDimensions((mxArray *)m0, iv1, 1);
  emlrtAssign(&y, m0);
  return y;
}

static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[25]
{
  real_T (*y)[25];
  y = o_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static real_T (*g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *arg_F,
  const char_T *identifier))[5]
{
  real_T (*y)[5];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = h_emlrt_marshallIn(sp, emlrtAlias(arg_F), &thisId);
  emlrtDestroyArray(&arg_F);
  return y;
}

static real_T (*h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[5]
{
  real_T (*y)[5];
  y = p_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static real_T (*i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *arg_A,
  const char_T *identifier))[1050]
{
  real_T (*y)[1050];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = j_emlrt_marshallIn(sp, emlrtAlias(arg_A), &thisId);
  emlrtDestroyArray(&arg_A);
  return y;
}

static real_T (*j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[1050]
{
  real_T (*y)[1050];
  y = q_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static real_T (*k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *arg_B,
  const char_T *identifier))[210]
{
  real_T (*y)[210];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = l_emlrt_marshallIn(sp, emlrtAlias(arg_B), &thisId);
  emlrtDestroyArray(&arg_B);
  return y;
}

static real_T (*l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[210]
{
  real_T (*y)[210];
  y = r_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static real_T m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  real_T ret;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, 0);
  ret = *(real_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[5])
{
  int32_T iv2[1];
  int32_T i1;
  iv2[0] = 5;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, iv2);
  for (i1 = 0; i1 < 5; i1++) {
    ret[i1] = (*(real_T (*)[5])mxGetData(src))[i1];
  }

  emlrtDestroyArray(&src);
}

static real_T (*o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[25]
{
  real_T (*ret)[25];
  int32_T iv3[2];
  int32_T i;
  for (i = 0; i < 2; i++) {
    iv3[i] = 5;
  }

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 2U, iv3);
  ret = (real_T (*)[25])mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  static real_T (*p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[5]
{
  real_T (*ret)[5];
  int32_T iv4[1];
  iv4[0] = 5;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, iv4);
  ret = (real_T (*)[5])mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static real_T (*q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[1050]
{
  real_T (*ret)[1050];
  int32_T iv5[2];
  int32_T i2;
  for (i2 = 0; i2 < 2; i2++) {
    iv5[i2] = 210 + -205 * i2;
  }

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 2U, iv5);
  ret = (real_T (*)[1050])mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  static real_T (*r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[210]
{
  real_T (*ret)[210];
  int32_T iv6[1];
  iv6[0] = 210;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, iv6);
  ret = (real_T (*)[210])mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

void solver_run_api(const mxArray * const prhs[7], const mxArray *plhs[1])
{
  real_T (*out_sol)[5];
  struct0_T obj;
  real_T (*arg_H)[25];
  real_T (*arg_F)[5];
  real_T (*arg_A)[1050];
  real_T (*arg_B)[210];
  real_T (*arg_p_min)[5];
  real_T (*arg_p_max)[5];
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;
  out_sol = (real_T (*)[5])mxMalloc(sizeof(real_T [5]));

  /* Marshall function inputs */
  emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "obj", &obj);
  arg_H = e_emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "arg_H");
  arg_F = g_emlrt_marshallIn(&st, emlrtAlias(prhs[2]), "arg_F");
  arg_A = i_emlrt_marshallIn(&st, emlrtAlias(prhs[3]), "arg_A");
  arg_B = k_emlrt_marshallIn(&st, emlrtAlias(prhs[4]), "arg_B");
  arg_p_min = g_emlrt_marshallIn(&st, emlrtAlias(prhs[5]), "arg_p_min");
  arg_p_max = g_emlrt_marshallIn(&st, emlrtAlias(prhs[6]), "arg_p_max");

  /* Invoke the target function */
  solver_run(&st, &obj, *arg_H, *arg_F, *arg_A, *arg_B, *arg_p_min, *arg_p_max, *
             out_sol);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(*out_sol);
}

/* End of code generation (_coder_solver_run_api.c) */
