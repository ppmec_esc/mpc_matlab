/*
 * _coder_solver_run_mex.c
 *
 * Code generation for function '_coder_solver_run_mex'
 *
 */

/* Include files */
#include "solver_run.h"
#include "_coder_solver_run_mex.h"
#include "solver_run_terminate.h"
#include "_coder_solver_run_api.h"
#include "solver_run_initialize.h"
#include "solver_run_data.h"

/* Function Declarations */
static void solver_run_mexFunction(int32_T nlhs, mxArray *plhs[1], int32_T nrhs,
  const mxArray *prhs[7]);

/* Function Definitions */
static void solver_run_mexFunction(int32_T nlhs, mxArray *plhs[1], int32_T nrhs,
  const mxArray *prhs[7])
{
  int32_T n;
  const mxArray *inputs[7];
  const mxArray *outputs[1];
  int32_T b_nlhs;
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 7) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 7, 4,
                        10, "solver_run");
  }

  if (nlhs > 1) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 10,
                        "solver_run");
  }

  /* Temporary copy for mex inputs. */
  for (n = 0; n < nrhs; n++) {
    inputs[n] = prhs[n];
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(&st);
    }
  }

  /* Call the function. */
  solver_run_api(inputs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);

  /* Module termination. */
  solver_run_terminate();
}

void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  /* Initialize the memory manager. */
  mexAtExit(solver_run_atexit);

  /* Module initialization. */
  solver_run_initialize();

  /* Dispatch the entry-point. */
  solver_run_mexFunction(nlhs, plhs, nrhs, prhs);
}

/* End of code generation (_coder_solver_run_mex.c) */
