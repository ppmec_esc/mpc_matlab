/*
 * _coder_solver_run_api.h
 *
 * Code generation for function '_coder_solver_run_api'
 *
 */

#ifndef ___CODER_SOLVER_RUN_API_H__
#define ___CODER_SOLVER_RUN_API_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "solver_run_types.h"

/* Function Declarations */
extern void solver_run_api(const mxArray * const prhs[7], const mxArray *plhs[1]);

#endif

/* End of code generation (_coder_solver_run_api.h) */
