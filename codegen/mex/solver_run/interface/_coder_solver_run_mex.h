/*
 * _coder_solver_run_mex.h
 *
 * Code generation for function '_coder_solver_run_mex'
 *
 */

#ifndef ___CODER_SOLVER_RUN_MEX_H__
#define ___CODER_SOLVER_RUN_MEX_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "solver_run_types.h"

/* Function Declarations */
extern void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const
  mxArray *prhs[]);

#endif

/* End of code generation (_coder_solver_run_mex.h) */
