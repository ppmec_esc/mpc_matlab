/*
 * solver_run.c
 *
 * Code generation for function 'solver_run'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "solver_run.h"
#include "solver_run_emxutil.h"
#include "norm.h"
#include "solver_run_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 31, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m" };

static emlrtRSInfo b_emlrtRSI = { 32, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m" };

static emlrtRSInfo c_emlrtRSI = { 49, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m" };

static emlrtRSInfo d_emlrtRSI = { 50, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m" };

static emlrtRSInfo e_emlrtRSI = { 53, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m" };

static emlrtRSInfo f_emlrtRSI = { 54, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m" };

static emlrtRSInfo ib_emlrtRSI = { 68, "eml_mtimes_helper",
  "C:\\Program Files\\MATLAB\\MATLAB Production Server\\R2015a\\toolbox\\eml\\lib\\matlab\\ops\\eml_mtimes_helper.m"
};

static emlrtRSInfo jb_emlrtRSI = { 54, "eml_xgemm",
  "C:\\Program Files\\MATLAB\\MATLAB Production Server\\R2015a\\toolbox\\eml\\lib\\matlab\\eml\\blas\\eml_xgemm.m"
};

static emlrtRSInfo kb_emlrtRSI = { 1, "xgemm",
  "C:\\Program Files\\MATLAB\\MATLAB Production Server\\R2015a\\toolbox\\coder\\coder\\+coder\\+internal\\+blas\\xgemm.p"
};

static emlrtRTEInfo emlrtRTEI = { 30, 22, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m" };

static emlrtRTEInfo b_emlrtRTEI = { 35, 5, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m" };

static emlrtRTEInfo c_emlrtRTEI = { 43, 5, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m" };

static emlrtBCInfo emlrtBCI = { -1, -1, 36, 12, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtBCInfo b_emlrtBCI = { -1, -1, 49, 30, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtBCInfo c_emlrtBCI = { -1, -1, 50, 26, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtBCInfo d_emlrtBCI = { -1, -1, 51, 19, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtBCInfo e_emlrtBCI = { -1, -1, 52, 19, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtBCInfo f_emlrtBCI = { -1, -1, 58, 20, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtBCInfo g_emlrtBCI = { -1, -1, 61, 20, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtBCInfo h_emlrtBCI = { -1, -1, 67, 54, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtBCInfo i_emlrtBCI = { -1, -1, 74, 21, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtBCInfo j_emlrtBCI = { -1, -1, 75, 22, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

static emlrtDCInfo emlrtDCI = { 35, 21, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 1 };

static emlrtDCInfo b_emlrtDCI = { 35, 21, "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 4 };

static emlrtBCInfo k_emlrtBCI = { -1, -1, 66, 20, "lesp", "solver_run",
  "C:\\git\\CP\\Matlab\\.\\@ClassPGE\\solver_run.m", 0 };

/* Function Definitions */
void solver_run(const emlrtStack *sp, struct0_T *obj, const real_T arg_H[25],
                const real_T arg_F[5], const real_T arg_A[1050], const real_T
                arg_B[210], const real_T arg_p_min[5], const real_T arg_p_max[5],
                real_T out_sol[5])
{
  real_T hmax0;
  real_T a[1050];
  int32_T i0;
  int32_T k;
  real_T alpha1;
  real_T beta1;
  char_T TRANSB;
  char_T TRANSA;
  real_T dv0[25];
  emxArray_real_T *lesp;
  ptrdiff_t m_t;
  ptrdiff_t n_t;
  ptrdiff_t k_t;
  ptrdiff_t lda_t;
  ptrdiff_t ldb_t;
  ptrdiff_t ldc_t;
  real_T hmaxg;
  real_T indrho;
  real_T d0;
  real_T d1;
  real_T gam;
  int32_T i;
  real_T hmax;
  real_T p1[5];
  real_T inter[210];
  real_T b_a;
  real_T inter2[210];
  real_T G[5];
  real_T y;
  real_T b_G;
  real_T b_y;
  real_T c_y[5];
  real_T maxval[210];
  real_T d_y;
  real_T e_y;
  real_T c_a;
  int32_T j;
  int32_T b_k;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);

  /* { */
  /* --------------------------------------------------------------------------- */
  /*   Criador: Zoé Magalhães (zr.magal@gmail.com) */
  /*   Mestrando do PPMEC-Unb, matrícula 170172767 */
  /*   Disciplina: Controle Preditivo 01/2018 */
  /*   Professor: André Murilo */
  /*    */
  /*   Função que executa a programação quadrática pelo algoritmo PGE. */
  /*   Histórico de modificações: */
  /* --------------------------------------------------------------------------- */
  /* - 28/06/2018 - Zoé Magalhães */
  /* - Início do controle de versão. */
  /* --------------------------------------------------------------------------- */
  /* } */
  /* { */
  /*     @brief Função que executa a programação quadrática pelo algoritmo */
  /*     PGE para minimizar a a função custo J = p'Hp + arg_F'p com as restrições */
  /*     Ap <= B, arg_p_min <= p <= arg_p_max; */
  /*     @param[in] arg_H é a matriz H da função custo */
  /*     @param[in] arg_F é a matriz arg_F da função custo */
  /*     @param[in] arg_A é matriz A da inequação de restrição */
  /*     @param[in] arg_B é a matriz B da inequação de restrição */
  /*     @param[in] arg_p_min é o limite inferior do espaço de busca */
  /*     @param[in] arg_p_max é o limite superior do espaço de bsuca */
  /*  */
  /* } */
  st.site = &emlrtRSI;
  hmax0 = norm(&st, arg_H);
  st.site = &b_emlrtRSI;
  for (i0 = 0; i0 < 210; i0++) {
    for (k = 0; k < 5; k++) {
      a[k + 5 * i0] = arg_A[i0 + 210 * k];
    }
  }

  b_st.site = &ib_emlrtRSI;
  c_st.site = &jb_emlrtRSI;
  d_st.site = &kb_emlrtRSI;
  alpha1 = 1.0;
  beta1 = 0.0;
  TRANSB = 'N';
  TRANSA = 'N';
  memset(&dv0[0], 0, 25U * sizeof(real_T));
  emxInit_real_T(&d_st, &lesp, 2, &b_emlrtRTEI, true);
  m_t = (ptrdiff_t)5;
  n_t = (ptrdiff_t)5;
  k_t = (ptrdiff_t)210;
  lda_t = (ptrdiff_t)5;
  ldb_t = (ptrdiff_t)210;
  ldc_t = (ptrdiff_t)5;
  dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &a[0], &lda_t, &arg_A[0],
        &ldb_t, &beta1, &dv0[0], &ldc_t);
  st.site = &b_emlrtRSI;
  hmaxg = norm(&st, dv0);
  indrho = 1.0;
  i0 = lesp->size[0] * lesp->size[1];
  lesp->size[0] = 5;
  d0 = obj->N_iter;
  if (d0 >= 0.0) {
  } else {
    d0 = emlrtNonNegativeCheckR2012b(d0, &b_emlrtDCI, sp);
  }

  if (d0 == (int32_T)muDoubleScalarFloor(d0)) {
    lesp->size[1] = (int32_T)d0;
  } else {
    lesp->size[1] = (int32_T)emlrtIntegerCheckR2012b(d0, &emlrtDCI, sp);
  }

  emxEnsureCapacity(sp, (emxArray__common *)lesp, i0, (int32_T)sizeof(real_T),
                    &emlrtRTEI);
  d0 = obj->N_iter;
  if (d0 >= 0.0) {
  } else {
    d0 = emlrtNonNegativeCheckR2012b(d0, &b_emlrtDCI, sp);
  }

  if (d0 == (int32_T)muDoubleScalarFloor(d0)) {
    d1 = d0;
  } else {
    d1 = emlrtIntegerCheckR2012b(d0, &emlrtDCI, sp);
  }

  k = 5 * (int32_T)d1;
  for (i0 = 0; i0 < k; i0++) {
    lesp->data[i0] = 0.0;
  }

  i0 = (int32_T)obj->N_iter;
  emlrtDynamicBoundsCheckR2012b(1, 1, i0, &emlrtBCI, sp);
  for (i0 = 0; i0 < 5; i0++) {
    lesp->data[i0] = obj->p0[i0];
  }

  gam = obj->gam_min;
  obj->rho = obj->rho0;
  d0 = obj->N_iter - 1.0;
  emlrtForLoopVectorCheckR2012b(1.0, 1.0, obj->N_iter - 1.0, mxDOUBLE_CLASS,
    (int32_T)(obj->N_iter - 1.0), &c_emlrtRTEI, sp);
  i = 0;
  while (i <= (int32_T)d0 - 1) {
    hmax = hmax0 + 2.0 * obj->rho * hmaxg;
    if (indrho == obj->n_rho) {
      obj->rho = muDoubleScalarMin(obj->rho_max, obj->beta_plus * obj->rho);
      indrho = 1.0;
    }

    st.site = &c_emlrtRSI;
    i0 = lesp->size[1];
    if ((i + 1 >= 1) && (i + 1 < i0)) {
      k = i + 1;
    } else {
      k = emlrtDynamicBoundsCheckR2012b(i + 1, 1, i0, &b_emlrtBCI, &st);
    }

    for (i0 = 0; i0 < 5; i0++) {
      p1[i0] = lesp->data[i0 + lesp->size[0] * (k - 1)];
    }

    b_st.site = &ib_emlrtRSI;
    c_st.site = &jb_emlrtRSI;
    d_st.site = &kb_emlrtRSI;
    alpha1 = 1.0;
    beta1 = 0.0;
    TRANSB = 'N';
    TRANSA = 'N';
    memset(&inter[0], 0, 210U * sizeof(real_T));
    m_t = (ptrdiff_t)210;
    n_t = (ptrdiff_t)1;
    k_t = (ptrdiff_t)5;
    lda_t = (ptrdiff_t)210;
    ldb_t = (ptrdiff_t)5;
    ldc_t = (ptrdiff_t)210;
    dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &arg_A[0], &lda_t, &p1[0],
          &ldb_t, &beta1, &inter[0], &ldc_t);
    b_a = 2.0 * obj->rho;
    for (i0 = 0; i0 < 210; i0++) {
      inter[i0] -= arg_B[i0];
      for (k = 0; k < 5; k++) {
        a[k + 5 * i0] = b_a * arg_A[i0 + 210 * k];
      }

      inter2[i0] = muDoubleScalarMax(0.0, inter[i0]);
    }

    i0 = lesp->size[1];
    k = i + 1;
    emlrtDynamicBoundsCheckR2012b(k, 1, i0, &c_emlrtBCI, sp);
    st.site = &d_emlrtRSI;
    b_st.site = &ib_emlrtRSI;
    c_st.site = &jb_emlrtRSI;
    d_st.site = &kb_emlrtRSI;
    alpha1 = 1.0;
    beta1 = 0.0;
    TRANSB = 'N';
    TRANSA = 'N';
    for (k = 0; k < 5; k++) {
      p1[k] = 0.0;
    }

    m_t = (ptrdiff_t)5;
    n_t = (ptrdiff_t)1;
    k_t = (ptrdiff_t)210;
    lda_t = (ptrdiff_t)5;
    ldb_t = (ptrdiff_t)210;
    ldc_t = (ptrdiff_t)5;
    dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &a[0], &lda_t, &inter2[0],
          &ldb_t, &beta1, &p1[0], &ldc_t);
    for (i0 = 0; i0 < 5; i0++) {
      y = 0.0;
      for (k = 0; k < 5; k++) {
        y += arg_H[i0 + 5 * k] * lesp->data[k + lesp->size[0] * i];
      }

      G[i0] = (y + arg_F[i0]) + p1[i0];
    }

    y = 1.0 / hmax;
    i0 = lesp->size[1];
    if ((i + 1 >= 1) && (i + 1 < i0)) {
      k = i + 1;
    } else {
      k = emlrtDynamicBoundsCheckR2012b(i + 1, 1, i0, &d_emlrtBCI, sp);
    }

    for (i0 = 0; i0 < 5; i0++) {
      p1[i0] = lesp->data[i0 + lesp->size[0] * (k - 1)] - y * G[i0];
    }

    y = gam / hmax;
    i0 = lesp->size[1];
    if ((i + 1 >= 1) && (i + 1 < i0)) {
      k = i + 1;
    } else {
      k = emlrtDynamicBoundsCheckR2012b(i + 1, 1, i0, &e_emlrtBCI, sp);
    }

    for (i0 = 0; i0 < 5; i0++) {
      b_G = lesp->data[i0 + lesp->size[0] * (k - 1)] - y * G[i0];
      G[i0] = b_G;
    }

    st.site = &e_emlrtRSI;
    b_st.site = &ib_emlrtRSI;
    c_st.site = &jb_emlrtRSI;
    d_st.site = &kb_emlrtRSI;
    alpha1 = 1.0;
    beta1 = 0.0;
    TRANSB = 'N';
    TRANSA = 'N';
    memset(&inter[0], 0, 210U * sizeof(real_T));
    m_t = (ptrdiff_t)210;
    n_t = (ptrdiff_t)1;
    k_t = (ptrdiff_t)5;
    lda_t = (ptrdiff_t)210;
    ldb_t = (ptrdiff_t)5;
    ldc_t = (ptrdiff_t)210;
    dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &arg_A[0], &lda_t, &p1[0],
          &ldb_t, &beta1, &inter[0], &ldc_t);
    for (i0 = 0; i0 < 210; i0++) {
      inter[i0] -= arg_B[i0];
    }

    st.site = &f_emlrtRSI;
    b_st.site = &ib_emlrtRSI;
    c_st.site = &jb_emlrtRSI;
    d_st.site = &kb_emlrtRSI;
    alpha1 = 1.0;
    beta1 = 0.0;
    TRANSB = 'N';
    TRANSA = 'N';
    memset(&inter2[0], 0, 210U * sizeof(real_T));
    m_t = (ptrdiff_t)210;
    n_t = (ptrdiff_t)1;
    k_t = (ptrdiff_t)5;
    lda_t = (ptrdiff_t)210;
    ldb_t = (ptrdiff_t)5;
    ldc_t = (ptrdiff_t)210;
    dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &arg_A[0], &lda_t, &G[0],
          &ldb_t, &beta1, &inter2[0], &ldc_t);
    for (i0 = 0; i0 < 210; i0++) {
      inter2[i0] -= arg_B[i0];
    }

    y = 0.0;
    b_y = 0.0;
    for (i0 = 0; i0 < 5; i0++) {
      c_y[i0] = 0.0;
      for (k = 0; k < 5; k++) {
        c_y[i0] += 0.5 * p1[k] * arg_H[k + 5 * i0];
      }

      y += c_y[i0] * p1[i0];
      b_y += arg_F[i0] * p1[i0];
    }

    for (k = 0; k < 210; k++) {
      maxval[k] = muDoubleScalarMax(0.0, inter[k]);
    }

    b_a = b_norm(maxval);
    d_y = 0.0;
    e_y = 0.0;
    for (i0 = 0; i0 < 5; i0++) {
      c_y[i0] = 0.0;
      for (k = 0; k < 5; k++) {
        c_y[i0] += 0.5 * G[k] * arg_H[k + 5 * i0];
      }

      d_y += c_y[i0] * G[i0];
      e_y += arg_F[i0] * G[i0];
    }

    for (k = 0; k < 210; k++) {
      maxval[k] = muDoubleScalarMax(0.0, inter2[k]);
    }

    c_a = b_norm(maxval);
    if ((y + b_y) + obj->rho * (b_a * b_a) < (d_y + e_y) + obj->rho * (c_a * c_a))
    {
      k = lesp->size[1];
      i0 = (int32_T)((1.0 + (real_T)i) + 1.0);
      emlrtDynamicBoundsCheckR2012b(i0, 1, k, &f_emlrtBCI, sp);
      for (i0 = 0; i0 < 5; i0++) {
        lesp->data[i0 + lesp->size[0] * ((int32_T)((1.0 + (real_T)i) + 1.0) - 1)]
          = p1[i0];
      }

      gam = muDoubleScalarMax(obj->gam_min, obj->beta_minus * gam);
    } else {
      k = lesp->size[1];
      i0 = (int32_T)((1.0 + (real_T)i) + 1.0);
      emlrtDynamicBoundsCheckR2012b(i0, 1, k, &g_emlrtBCI, sp);
      for (i0 = 0; i0 < 5; i0++) {
        lesp->data[i0 + lesp->size[0] * ((int32_T)((1.0 + (real_T)i) + 1.0) - 1)]
          = G[i0];
      }

      gam *= obj->beta_plus;
    }

    j = 0;
    while (j < 5) {
      i0 = lesp->size[1];
      k = (int32_T)((1.0 + (real_T)i) + 1.0);
      emlrtDynamicBoundsCheckR2012b(k, 1, i0, &h_emlrtBCI, sp);
      i0 = lesp->size[1];
      k = (int32_T)((1.0 + (real_T)i) + 1.0);
      if ((k >= 1) && (k < i0)) {
        b_k = k;
      } else {
        b_k = emlrtDynamicBoundsCheckR2012b(k, 1, i0, &k_emlrtBCI, sp);
      }

      lesp->data[j + lesp->size[0] * (b_k - 1)] = muDoubleScalarMin(arg_p_max[j],
        muDoubleScalarMax(arg_p_min[j], lesp->data[j + lesp->size[0] * ((int32_T)
        ((1.0 + (real_T)i) + 1.0) - 1)]));
      j++;
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    indrho++;

    /* disp(obj.J); */
    i++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  i0 = lesp->size[1];
  k = lesp->size[1];
  emlrtDynamicBoundsCheckR2012b(k, 1, i0, &i_emlrtBCI, sp);
  i0 = lesp->size[1];
  k = lesp->size[1];
  if ((k >= 1) && (k < i0)) {
  } else {
    k = emlrtDynamicBoundsCheckR2012b(k, 1, i0, &j_emlrtBCI, sp);
  }

  for (i0 = 0; i0 < 5; i0++) {
    out_sol[i0] = lesp->data[i0 + lesp->size[0] * (k - 1)];
  }

  emxFree_real_T(&lesp);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (solver_run.c) */
