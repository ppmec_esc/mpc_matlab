/*
 * solver_run_terminate.h
 *
 * Code generation for function 'solver_run_terminate'
 *
 */

#ifndef __SOLVER_RUN_TERMINATE_H__
#define __SOLVER_RUN_TERMINATE_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "solver_run_types.h"

/* Function Declarations */
extern void solver_run_atexit(void);
extern void solver_run_terminate(void);

#endif

/* End of code generation (solver_run_terminate.h) */
