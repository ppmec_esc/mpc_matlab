/*
 * solver_run_initialize.c
 *
 * Code generation for function 'solver_run_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "solver_run.h"
#include "solver_run_initialize.h"
#include "solver_run_data.h"

/* Function Definitions */
void solver_run_initialize(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/* End of code generation (solver_run_initialize.c) */
