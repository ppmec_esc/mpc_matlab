/*
 * solver_run.h
 *
 * Code generation for function 'solver_run'
 *
 */

#ifndef __SOLVER_RUN_H__
#define __SOLVER_RUN_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "solver_run_types.h"

/* Function Declarations */
extern void solver_run(const emlrtStack *sp, struct0_T *obj, const real_T arg_H
  [25], const real_T arg_F[5], const real_T arg_A[1050], const real_T arg_B[210],
  const real_T arg_p_min[5], const real_T arg_p_max[5], real_T out_sol[5]);

#endif

/* End of code generation (solver_run.h) */
