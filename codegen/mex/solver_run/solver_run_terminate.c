/*
 * solver_run_terminate.c
 *
 * Code generation for function 'solver_run_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "solver_run.h"
#include "solver_run_terminate.h"
#include "solver_run_data.h"

/* Function Definitions */
void solver_run_atexit(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

void solver_run_terminate(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (solver_run_terminate.c) */
