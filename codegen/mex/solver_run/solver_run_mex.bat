@echo off
set MATLAB=C:\PROGRA~1\MATLAB\MATLAB~1\R2015a
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\MATLAB Production Server\R2015a\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=solver_run_mex
set MEX_NAME=solver_run_mex
set MEX_EXT=.mexw64
call setEnv.bat
echo # Make settings for solver_run > solver_run_mex.mki
echo COMPILER=%COMPILER%>> solver_run_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> solver_run_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> solver_run_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> solver_run_mex.mki
echo LINKER=%LINKER%>> solver_run_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> solver_run_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> solver_run_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> solver_run_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> solver_run_mex.mki
echo BORLAND=%BORLAND%>> solver_run_mex.mki
echo OMPFLAGS= >> solver_run_mex.mki
echo OMPLINKFLAGS= >> solver_run_mex.mki
echo EMC_COMPILER=msvc120>> solver_run_mex.mki
echo EMC_CONFIG=optim>> solver_run_mex.mki
"C:\Program Files\MATLAB\MATLAB Production Server\R2015a\bin\win64\gmake" -B -f solver_run_mex.mk
