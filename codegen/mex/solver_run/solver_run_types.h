/*
 * solver_run_types.h
 *
 * Code generation for function 'solver_run'
 *
 */

#ifndef __SOLVER_RUN_TYPES_H__
#define __SOLVER_RUN_TYPES_H__

/* Include files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef struct_emxArray__common
#define struct_emxArray__common

struct emxArray__common
{
  void *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray__common*/

#ifndef typedef_emxArray__common
#define typedef_emxArray__common

typedef struct emxArray__common emxArray__common;

#endif                                 /*typedef_emxArray__common*/

#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real_T*/

#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T

typedef struct emxArray_real_T emxArray_real_T;

#endif                                 /*typedef_emxArray_real_T*/

#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  real_T rho0;
  real_T rho_max;
  real_T N_iter;
  real_T p0[5];
  real_T gam_min;
  real_T beta_plus;
  real_T beta_minus;
  real_T n_rho;
  real_T rho;
} struct0_T;

#endif                                 /*typedef_struct0_T*/
#endif

/* End of code generation (solver_run_types.h) */
