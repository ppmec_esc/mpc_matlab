/*
 * model_update.c
 *
 * Code generation for function 'model_update'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "model_update.h"

/* Function Definitions */
void model_update(const real_T arg_A[16], const real_T arg_B[8], const real_T
                  arg_x[4], real_T arg_u, real_T arg_e, real_T out_x[4])
{
  real_T b_arg_u[2];
  real_T b_arg_A[4];
  real_T b_arg_B[4];
  int32_T i0;
  int32_T i1;
  b_arg_u[0] = arg_u;
  b_arg_u[1] = arg_e;
  for (i0 = 0; i0 < 4; i0++) {
    b_arg_A[i0] = 0.0;
    for (i1 = 0; i1 < 4; i1++) {
      b_arg_A[i0] += arg_A[i0 + (i1 << 2)] * arg_x[i1];
    }

    b_arg_B[i0] = 0.0;
    for (i1 = 0; i1 < 2; i1++) {
      b_arg_B[i0] += arg_B[i0 + (i1 << 2)] * b_arg_u[i1];
    }

    out_x[i0] = b_arg_A[i0] + b_arg_B[i0];
  }
}

/* End of code generation (model_update.c) */
