/*
 * model_update_terminate.c
 *
 * Code generation for function 'model_update_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "model_update.h"
#include "model_update_terminate.h"
#include "model_update_data.h"

/* Function Definitions */
void model_update_atexit(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

void model_update_terminate(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (model_update_terminate.c) */
