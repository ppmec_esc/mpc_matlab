/*
 * model_update.h
 *
 * Code generation for function 'model_update'
 *
 */

#ifndef __MODEL_UPDATE_H__
#define __MODEL_UPDATE_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "model_update_types.h"

/* Function Declarations */
extern void model_update(const real_T arg_A[16], const real_T arg_B[8], const
  real_T arg_x[4], real_T arg_u, real_T arg_e, real_T out_x[4]);

#endif

/* End of code generation (model_update.h) */
