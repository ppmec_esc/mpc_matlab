/*
 * _coder_model_update_api.h
 *
 * Code generation for function '_coder_model_update_api'
 *
 */

#ifndef ___CODER_MODEL_UPDATE_API_H__
#define ___CODER_MODEL_UPDATE_API_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "model_update_types.h"

/* Function Declarations */
extern void model_update_api(const mxArray * const prhs[5], const mxArray *plhs
  [1]);

#endif

/* End of code generation (_coder_model_update_api.h) */
