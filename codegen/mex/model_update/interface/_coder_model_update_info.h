/*
 * _coder_model_update_info.h
 *
 * Code generation for function 'model_update'
 *
 */

#ifndef ___CODER_MODEL_UPDATE_INFO_H__
#define ___CODER_MODEL_UPDATE_INFO_H__
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);

#endif
/* End of code generation (_coder_model_update_info.h) */
