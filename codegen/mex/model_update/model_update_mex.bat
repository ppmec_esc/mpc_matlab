@echo off
set MATLAB=C:\PROGRA~1\MATLAB\MATLAB~1\R2015a
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\MATLAB Production Server\R2015a\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=model_update_mex
set MEX_NAME=model_update_mex
set MEX_EXT=.mexw64
call setEnv.bat
echo # Make settings for model_update > model_update_mex.mki
echo COMPILER=%COMPILER%>> model_update_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> model_update_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> model_update_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> model_update_mex.mki
echo LINKER=%LINKER%>> model_update_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> model_update_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> model_update_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> model_update_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> model_update_mex.mki
echo BORLAND=%BORLAND%>> model_update_mex.mki
echo OMPFLAGS= >> model_update_mex.mki
echo OMPLINKFLAGS= >> model_update_mex.mki
echo EMC_COMPILER=msvc120>> model_update_mex.mki
echo EMC_CONFIG=optim>> model_update_mex.mki
"C:\Program Files\MATLAB\MATLAB Production Server\R2015a\bin\win64\gmake" -B -f model_update_mex.mk
