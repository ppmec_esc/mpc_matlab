/*
 * model_update_initialize.c
 *
 * Code generation for function 'model_update_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "model_update.h"
#include "model_update_initialize.h"
#include "model_update_data.h"

/* Function Definitions */
void model_update_initialize(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/* End of code generation (model_update_initialize.c) */
