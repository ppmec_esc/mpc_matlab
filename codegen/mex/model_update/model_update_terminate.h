/*
 * model_update_terminate.h
 *
 * Code generation for function 'model_update_terminate'
 *
 */

#ifndef __MODEL_UPDATE_TERMINATE_H__
#define __MODEL_UPDATE_TERMINATE_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "model_update_types.h"

/* Function Declarations */
extern void model_update_atexit(void);
extern void model_update_terminate(void);

#endif

/* End of code generation (model_update_terminate.h) */
