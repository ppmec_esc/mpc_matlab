/*
 * model_update_data.h
 *
 * Code generation for function 'model_update_data'
 *
 */

#ifndef __MODEL_UPDATE_DATA_H__
#define __MODEL_UPDATE_DATA_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "model_update_types.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern emlrtContext emlrtContextGlobal;

#endif

/* End of code generation (model_update_data.h) */
