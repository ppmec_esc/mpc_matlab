/*
 * model_update_data.c
 *
 * Code generation for function 'model_update_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "model_update.h"
#include "model_update_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar;
emlrtContext emlrtContextGlobal = { true, false, 131418U, NULL, "model_update",
  NULL, false, { 2045744189U, 2170104910U, 2743257031U, 4284093946U }, NULL };

/* End of code generation (model_update_data.c) */
