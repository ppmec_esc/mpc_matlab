function cost = costfunc2(vars)

    persistent globalcost;
    persistent globalsol;
    
    if isempty(globalcost)
        globalcost = 1e20;
        globalsol = 0;
    end

    newvars = [vars(1:2) vars(3)*0.2];
    cost =  costfunc(newvars,3,false,false,false,15);
    cost =  cost + costfunc(newvars,2,false,false,false,15)/3;   
    if cost < globalcost
        globalsol = newvars;
        globalcost = cost;
    end 
    
     globalsol
     globalcost
        
end
