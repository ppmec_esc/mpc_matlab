function plotpath(simresult,speedid,open_loop)

    figure(1000 + speedid);

    hold off
    x = simresult.path(:,2);
    y = simresult.path(:,1);
    hold on;
       
    if open_loop == 0
        plot(x,y,'Color',[0.4660, 0.4740, 0.1880]);    
         plot(x,simresult.target_path,'--','Color',[0.7 0.7 0.7]);
         plot(x,simresult.target_path_lb,'--k');
         plot(x,simresult.target_path_ub,'--k');
        %axis([0 200 0 100]);
    else
        plot(x,y,'Color',[0.5, 0.5, 0.5]);    
    end
    
    xlabel('longitudinal displacement $x$ (m)','FontSize',30)
    ylabel('vertical displacement $y$ (m)','FontSize',30)
    set(gca,'FontSize', 30)
end
