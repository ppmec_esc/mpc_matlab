load vehset
speedid = 2;
openloop = false;
simresult.time = hiltime;
speed = 100/3.6;
simresult.side_slip = atan2(hiloutput(:,2),speed);
simresult.yaw_rate = hiloutput(:,3);
simresult.roll = hiloutput(:,5);
simresult.steer = hiloutput(:,11);
simresult.states = hiloutput(:,2:5);
mu_g =  7.354987500000000;
L = 2.4;
Ku = 9.475348465328343e-05;
simresult.ref_states = desired_states(simresult.steer,speed,mu_g,L,Ku);
simresult.yaw_moment = hiloutput(:,1);
simresult.diff_yaw_moment = [0 ;diff(simresult.yaw_moment)];
plotnonlinearresult