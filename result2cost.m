function cost = result2cost(simresult)
    cost = 0;
    if find(simresult.path(:,1) > simresult.target_path_ub)
        cost = 5e6;
        error = 'meneuver out of up bound'
    end

    if find(simresult.path(:,1) < simresult.target_path_lb)
        cost = 5e6;
        error = 'maneuver out of low bound'
    end

    if find(simresult.yaw_moment>250) 
        cost = 5e6;
    else
        if find(simresult.yaw_moment>150)
            cost = 5e5;
        end
    end

    costs = (trapz(abs(simresult.ref_states(:,1) - simresult.yaw_rate))...
          +trapz(abs(simresult.side_slip)));

    mse = trapz(simresult.time,(simresult.path(:,1) - simresult.target_path).^2)/simresult.time(end);

    cost =  (cost + mse*100 + costs)*1e-6;
end