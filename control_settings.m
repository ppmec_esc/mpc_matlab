function out = control_settings(...
    a,... %length from cg to front axix
    b,... %length from cg to rear axix
    t_f,... %front track, length of front axix
    t_r,... %rear track, length of rear axix
    hg,... %heigh of cg
    m,... %total mass
    hs,... %heigh from roll center to cg
    ms,... %sprung mass, mass above roll center
    kf,... %front roll stiffness coefficient
    kr,... %rear roll stiffness coefficient
    cf,... %front roll damping coefficient
    cr,... %rear roll damping coefficient
    roll_damping,... %total roll damping
    roll_stiffness,... %total roll stiffness
    Izz,... %Yaw inertial moment
    Ixx,... %Roll inertial moment
    Ixz,... %Inertial product between roll and yaw axes
    g,... %Gravitational acceleration
    L,... %Length from rear to front axis
    MFA,... %coefficient of magic formula for lateral force
    MFB,... %coefficients of magic formula for longitudinal force
    Reff,... % wheel effective radio
    Iw,... % wheel inertial moment
    camber_by_roll, ... % camber by roll ratio
    friction, ... % tire-road friction coefficients
    long_speed, ... % initial longitudinal speed
    Q_Y,... % matrix that weighs states erros
    Q_U,... % matrix that weighs command energy
    Tcontrol, ... % control update period
    yaw_rate_err_th,... % yaw rate error threshold for control activation
    beta_th,... %side slip threshold for control activation
    deactive_periods,... % period withou activation criteria satisfaction in which control remains active
    lb,... %slip lower constraint
    ub,... %slip upper constraint
    slew_lb,... %minimum command slew rate
    slew_ub,... %maximum command slew rate
    cmd_lb,... %minimum command
    cmd_ub,... %maximum command
    Horizon,... %Prediction horizon 
    C_C,... %Constraint matrix, constraint states are y_c = C_C x
    param_coeff,... %Coefficients of exp. param of MCP
    dc_level,... %Enable dc level in exp param of MPC
    is_roll_enabled, ... %when true LQR controls roll error
    controlled_states ... %selects the controlled states
)

    Fze = ([b,b,a,a]*m*g/(2000*L));
    BCD = MFA(4)*sin(2*atan(Fze/MFA(5)))*180/pi;
    C = MFA(1);
    D = friction*MFA(2)*Fze.*Fze + MFA(3)*Fze;
    B = BCD/(C*D);
    E = MFA(7)*Fze + MFA(8);
    alpha_0 = (MFA(10)*Fze + MFA(11))*pi/180;
    
    k_0 = cos(C*atan(B.*(1-E).*alpha_0 + E.*atan(B.*alpha_0)));
    den1 = 1./(1+(B.*(1-E).*alpha_0+E.*atan(B.*alpha_0)).^2);
    den2 = 1./(1+(B.*alpha_0).^2);
    C_lambda = ((pi/180)*MFA(9)*BCD.*k_0.*(1+E.*(-1+1./den2))./den1 + Fze.*(MFA(12)*Fze + MFA(13)));
    C_alpha = BCD.*(1+E.*((1./den2)-1))./den1;
    %C_alpha = friction*MFA(4)*sind( 2*atand( Fze /MFA(5) ) )/(pi/180);
    
    out.C_alpha = C_alpha;
    out.C_lambda = C_lambda;
    out.Ku =abs(m*2*((a/(out.C_alpha(3)+out.C_alpha(4))) - (b/(out.C_alpha(1)+out.C_alpha(2))))/(L*L));
    out.mu_g = friction*g;
    
    %% linear model M1 x'= A1 x + B1 u + E1 steer
    M1 = [ m*long_speed,       0,   -ms*hs, 0;
           0,                          Izz, -Ixz,   0;
           -ms*hs*long_speed, -Ixz, Ixx,    roll_damping;
           0,                          0,   0,      1 ];
    
    A1 = [ -sum(C_alpha),        -(C_alpha*[a;a;-b;-b]/long_speed) - m*long_speed, 0,   sum(C_lambda)*camber_by_roll;
           -C_alpha*[a;a;-b;-b], -C_alpha*[a*a;a*a;b*b;b*b]/long_speed,            0, C_lambda*[a;a;-b;-b]*camber_by_roll;
           0,                    ms*hs*long_speed,                                 0, (ms*hs*g-roll_stiffness);
           0,                    0,                                                 1, 0 ];
    
    B1 = [0; 1; 0; 0];  
    
    E1 = [ C_alpha*[1;1;0;0]; C_alpha*[a;a;0;0];0;0 ];
    
    if ~is_roll_enabled
        M1=M1(1:2,1:2);
        A1=A1(1:2,1:2);
        B1=B1(1:2);
        E1=E1(1:2);
    end
    
    %% Modelo espa�o de estado: x` = Ax + Bu + Esteer y'= Cx + Du
    A = M1\A1;
    B = M1\B1;
    C = eye(size(A));
    E = M1\E1;
    D = zeros(size([B E]));
    
    sys = ss( A,[B E],C,D);
    d_sys =c2d(sys,Tcontrol);
    out.dA = d_sys.a;
    out.dB = d_sys.b;
    out.dC = d_sys.c;
    out.dD = d_sys.d;
    
    % Test if linear model is stable, it is expected to be stable
    out.eig = eig(d_sys.a);
    if find( abs(eig(d_sys.a)) > 1.0 )
        out.unstable = true
        out.mpc = 0;
    else
        
        if is_roll_enabled
            C_R = d_sys.c(controlled_states,:);
        else
            C_R = d_sys.c(controlled_states(controlled_states<3),:);
        end
        
    	out.unstable = false;
        mpcobj = ...
	    ClassLinearMPC.settings(...
	       d_sys.a,d_sys.b(:,1:end-size(E,2)), C_R, d_sys.d(:,1:end-size(E,2)),...
               Q_U,Q_Y, C_C, Horizon, lb, ub, slew_lb,slew_ub, cmd_lb, cmd_ub,...
	       d_sys.b(:,end-size(E,2)+1:end),'exponencial', param_coeff, dc_level, Tcontrol );
        out.mpc.G1 = mpcobj.G1;
        out.mpc.G2 = mpcobj.G2;
        out.mpc.G3 = mpcobj.G3;
        out.mpc.G4 = mpcobj.G4;
        out.mpc.F1 = mpcobj.F1;
        out.mpc.F2 = mpcobj.F2;
        out.mpc.F3 = mpcobj.F3;
        out.mpc.AINEQ = mpcobj.AINEQ();
        out.mpc.H = mpcobj.H;
        size(out.mpc.H)
        out.mpc.np = mpcobj.np;
        out.mpc.nc = mpcobj.nc;
        out.mpc.nu = mpcobj.nu;
        out.mpc.nv = mpcobj.nv;
        out.mpc.nuc = mpcobj.nu - mpcobj.nv;
        out.mpc.N = Horizon;
        out.mpc.ns = size(A,2);
        out.mpc.PI = mpcobj.PI_E;
    end;

    out.a = a;
    out.b = b;
    out.t_f = t_f;
    out.t_r = t_r;
    out.hg = hg;
    out.m = m;
    out.hs = hs;
    out.ms = ms;
    out.kf = kf;
    out.kr = kr;
    out.cf = cf;
    out.cr = cr;
    out.roll_damping = roll_damping;
    out.roll_stiffness = roll_stiffness;
    out.Izz = Izz;
    out.Ixx = Ixx;
    out.Ixz = Ixz;
    out.g = g;
    out.L = L;
    out.MFA = MFA;
    out.MFB = MFB;
    out.Reff = Reff;
    out.Iw = Iw;
    out.camber_by_roll = camber_by_roll;
    out.friction = friction;
    out.yaw_rate_err_th=yaw_rate_err_th;
    out.beta_th=beta_th;
    out.deactive_periods=deactive_periods;
    out.lb=lb;
    out.ub=ub;
    out.slew_lb=slew_lb;
    out.slew_ub=slew_ub;
    out.cmd_lb=cmd_lb;
    out.cmd_ub=cmd_ub;
    out.long_speed = long_speed;
    out.Tcontrol = Tcontrol;
    out.is_roll_enabled = is_roll_enabled;
end
