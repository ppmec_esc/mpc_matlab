function tire_slip = gettireslip(steer,lat_speed,yaw_rate,veh)

tire_slip(:,1) = steer - atan2(lat_speed+veh.a*yaw_rate,veh.long_speed - veh.t_f*yaw_rate/2);
tire_slip(:,2) = steer - atan2(lat_speed+veh.a*yaw_rate,veh.long_speed + veh.t_f*yaw_rate/2);
tire_slip(:,3) = - atan2(lat_speed-veh.b*yaw_rate,veh.long_speed - veh.t_r*yaw_rate/2);
tire_slip(:,4) = - atan2(lat_speed-veh.b*yaw_rate,veh.long_speed + veh.t_r*yaw_rate/2);
end