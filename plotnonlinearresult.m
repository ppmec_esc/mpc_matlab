set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
set(groot, 'DefaultLineLineWidth', 4);
set(gcf,'GraphicsSmoothing','off')

figure()


if open_loop == 1
    color = [0.5 0.5 0.5];
else
    color = [0.4660, 0.2740, 0.1880];
    color2 = [0.2660, 0.0740, 0.0];
end


%% PLOT OF SLIP ANGLE
subplot(2,4,1)
   %hold off
   t = simresult.time;
   slip_angle = simresult.side_slip*180/pi;
   plot( t,slip_angle,'Color',color);
   hold on
   %if open_loop = 0
   %plot( t,lb*ones(length(t))*180/pi);
   %plot( t,ub*ones(length(t))*180/pi);
   %else
   %title('Side-slip angle','FontSize',24)
   %xlabel('time(s)','FontSize',24)
   ylabel('$\beta(deg)$','FontSize',24)
   set(gca,'FontSize', 24)
   axis([0 15 -10 10]);
       

%% PLOT OF YAW RATE
subplot(2,4,2)
  % hold off
   yaw_rate = simresult.yaw_rate*180/pi;
   plot( simresult.time, yaw_rate,'Color',color);
   hold on
   if open_loop == 0
   ref_yaw_rate = simresult.ref_states*180/pi;
   plot( t, ref_yaw_rate,'--','Color',color2);
   end
   %title('Yaw rate','FontSize',24)
   %xlabel('time(s)','FontSize',24)
   ylabel('$\dot\psi$(deg/s)','FontSize',24)
   set(gca,'FontSize', 24)
   axis([0 15 -30 30]);

%% PLOT OF ROLL RATE
subplot(2,4,3)
  % hold off
   roll = simresult.roll*180/pi;
   plot( t, roll,'Color',color);
   hold on
   %title('Roll angle','FontSize',24)
   %xlabel('time(s)','FontSize',24)
   ylabel('$\phi$(deg)','FontSize',24)
   set(gca,'FontSize', 24)
   axis([0 15 -5 5]);

%% PLOT OF RIGHT TIRE SLIP
 subplot(2,4,4)
    tire_slip = gettireslip(simresult.steer,simresult.states(:,1),simresult.states(:,2),veh_model);
    hold off
    plot(t,tire_slip(:,[2])*180/pi,'--','Color',color);
    hold on
    plot(t,tire_slip(:,[4])*180/pi,'Color',color);
    
    %title('Right Tires slip angle','FontSize',24)
    %xlabel('time(s)','FontSize',24)
    ylabel('$\alpha(deg)$','FontSize',24)
    set(gca,'FontSize', 24)
    axis([0 15 -10 20]);

%% PLOT OF STEERING WHEEL COMMAND
subplot(2,4,5)
   %hold off
   %plot(t,simresult.steer*24*180/pi,'Color',color);
   plot(t,simresult.steer*180/pi,'Color',color);
   hold on
   %title('steering wheel command','FontSize',24)
   %%xlabel('time(s)','FontSize',24)
   ylabel('$\delta_D(deg)$','FontSize',24)
   set(gca,'FontSize', 24)
   axis([0 15 -100 100]);

%% PLOT OF  COMMAND
subplot(2,4,6)
 %  hold off
   if open_loop == 0
   plot(t, simresult.yaw_moment,'Color',color);
   hold on
   plot(t,cmd_lb(1)*ones(size(t)),'--k');
   plot(t,cmd_ub(1)*ones(size(t)),'--k');
   end
   %title('ESC command','FontSize',24)
   set(gca,'FontSize', 24)
   %%xlabel('time(s)','FontSize',24)
   ylabel('$M_u(Nm)$','FontSize',24)
   axis([0 15 -400 400]);

%% PLOT OF COMMAND RATE
subplot(2,4,7)
   %hold off
   if open_loop == 0
   plot(t, simresult.diff_yaw_moment/(1000*Tcontrol),'Color',color);
   hold on
   plot(t,slew_lb(1)*ones(size(t))/(1000*Tcontrol),'--k');
   plot(t,slew_ub(1)*ones(size(t))/(1000*Tcontrol),'--k');
   end
   %title('ESC command slew rate','FontSize',24)
   
   set(gca,'FontSize', 24)
   %%xlabel('time(s)','FontSize',24)
   ylabel('$\dot{M_u}(KNm/s)$','FontSize',24)
   axis([0 15 -3 3]);

%% PLOT OF TIRE LEFT SLIP
subplot(2,4,8)
   tire_slip = gettireslip(simresult.steer,simresult.states(:,1),simresult.states(:,2),veh_model);
  % hold off
   plot(t,tire_slip(:,[1])*180/pi,'--','Color',color);
   hold on
   plot(t,tire_slip(:,[3])*180/pi,'Color',color);
   set(gca,'FontSize', 24)

   %title('Left tires slip angle','FontSize',24)
   %xlabel('time(s)','FontSize',24)
   ylabel('$\alpha(deg)$','FontSize',24)
   set(gca,'FontSize', 24)
   axis([0 15 -10 20]);
   set(gca,'FontSize', 24)


