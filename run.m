function out = run(veh,driver,vehcontrol,path,path_lb,path_ub,Ts,openloop,T,prof_enabled)
%  function out = run(veh,driver,vehcontrol)
% veh is the vehicle model setted up by function vehicle_settings
% driver is the driver model setted up by function driver_settings
% vehcontrol is the control model setted by function control_settings
% path,path_lb and path_ub are the desired trajectory points and its constraints
% setted up by pathgen
    if vehcontrol.unstable
        discret_ss_unstable = abs(eig(d_sys.a))
        return
    end
    
    persistent simopt;
    if isempty(simopt) 
        simopt = simset('SrcWorkspace','Current','DstWorkspace','Current' );
    end
    
    VY_GAIN = driver.VY_GAIN;
    VY_FB = driver.VY_FB;
    YAW_FB = driver.YAW_FB;
    PREVIEWLOG = driver.PREVIEWLOG;
    params = veh.params;
    C_alpha = vehcontrol.C_alpha;			   
    mu_g = vehcontrol.mu_g;
    Ku = vehcontrol.Ku; 
    model_long_speed = veh.long_speed;    
    Tcontrol = vehcontrol.Tcontrol; 
    L = veh.L;	
    beta_th = vehcontrol.beta_th;
    yaw_rate_err_th = vehcontrol.yaw_rate_err_th;
    deactive_periods = vehcontrol.deactive_periods; 
    is_roll_enabled = vehcontrol.is_roll_enabled;
    G1 = vehcontrol.mpc.G1;
    G2 = vehcontrol.mpc.G2;
    G3=vehcontrol.mpc.G3;
    F1=vehcontrol.mpc.F1;
    F2=vehcontrol.mpc.F2;
    F3=vehcontrol.mpc.F3;
    cmd_lb=vehcontrol.cmd_lb;
    cmd_ub=vehcontrol.cmd_ub;
    N = vehcontrol.mpc.N;
    nuc = vehcontrol.mpc.nuc;
    npc = vehcontrol.mpc.np;
    H=vehcontrol.mpc.H;
    AINEQ=vehcontrol.mpc.AINEQ;
    PI=vehcontrol.mpc.PI;
    ub=vehcontrol.ub;
    lb=vehcontrol.lb;
    nc=vehcontrol.mpc.nc;
    slew_ub = vehcontrol.slew_ub;
    slew_lb = vehcontrol.slew_lb;
    
    maxsteer = pi/80 ;
    simin(:,1) = 0:0.1:T;
    simin(1:90,2) = 0;
    simin(91:100,2) = [maxsteer/10:maxsteer/10:maxsteer];
    simin(101:250,2) = maxsteer;
    simin(251:300,2) = [maxsteer-maxsteer/50:-maxsteer/50:0];
    simin(301:end,2) = 0;
    

    load_system('vscmpc')    
    
    if prof_enabled
       set_param('vscmpc','Profile','on')
    else
       set_param('vscmpc','Profile','off')
    end
    
    sim('vscmpc',[0 T],simopt);
    
    maxstates = [max(veh_states.Data) max(steer_angle.Data)];
    minstates = [min(veh_states.Data) min(steer_angle.Data)];
    meanstates = (maxstates + minstates)/2;
    minstates = 2*minstates - meanstates;
    maxstates = meanstates + 1.1*(maxstates-meanstates);
    minstates = meanstates - 1.1*(meanstates-minstates);
    
    %remove padding of delay in driver path preview
    ind = find(target_path_ub.Data ~= 0);
    pad_id = ind(1);

    out.time = veh_states.Time;
    out.lat_speed = veh_states.Data(:,1);
    out.yaw_rate = veh_states.Data(:,2);
    out.ref_states = ref_states.Data;
    out.side_slip = side_slip.Data;
    out.roll = veh_states.Data(:,4);
    
    
    out.yaw_moment = yaw_moment_control_signal.Data;
    out.diff_yaw_moment = [0 ;diff(out.yaw_moment)];
        
    
    out.steer = steer_angle.Data;
    out.target_path = target_path.Data;
    out.target_path_lb = target_path_lb.Data;
    out.target_path_ub = target_path_ub.Data;
    out.target_path_lb(out.target_path_lb==0) = out.target_path_lb(pad_id);
    out.target_path_ub(out.target_path_ub==0) = out.target_path_ub(pad_id);
    out.path = actual_path.Data;
    out.states = veh_states.Data;
    out.constub = constub.Data;
    out.constlb = constlb.Data;
%    out.lat_accel = lat_accel.Data;
    
end
