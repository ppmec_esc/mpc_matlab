%% Configuração

    % config speed
    % 1 = 80km/h ,2 = 100 km/h, 3 = 120 km/h
    speed_vector = [50,100,120];
    speed = 3;
    speed_vector(speed)
    plot_always = true
    div_param = false
    open_loop = false
    simulation_time = 35
    mpc_horizon = 50
    is_roll_enabled = true
    prof_enabled = false;
    

    if is_roll_enabled
       opt = 1e4*[7.050999026466902   0.649943973415584   0.02*5.512561895209386   0.02*5.583354859262296];
    else
       opt = 1e5*[1.0000    0.0849    0.2000];
    end

    cost = costfunc(opt,speed,plot_always,div_param,open_loop,simulation_time,...
                    mpc_horizon,is_roll_enabled,prof_enabled);
    
    return
   
